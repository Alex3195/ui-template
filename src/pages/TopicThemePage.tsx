import React from "react";
import TopicThemeTable from "../components/table/topicTheme/TopicThemeTable";

function TopicThemePage() {
  return (
    <>
      <TopicThemeTable />
    </>
  );
}

export default TopicThemePage;
