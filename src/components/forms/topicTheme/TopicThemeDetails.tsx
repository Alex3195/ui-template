import { Backspace } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router";
import { useAppSelector } from "../../../app/hooks";
import { selectLanguage } from "../../../features/lang/languageSlice";
import { ImageSettingDto } from "../../../features/user/imageSettingDto";
import { APIKit } from "../../../request/requests";
import { TopicTheme } from "../../../types/TopicTheme";
import { ModelCategory } from "../../../types/ModelCategory";
import { Model } from "../../../types/Model";
import MultiselectComponent from "../../MultiSelect/MultiSelectComponent";

async function uploadAsync(file: File, type: string) {
  let formData = new FormData();
  formData.append("image", file);

  let res = await APIKit.post("/image/file", formData);
  return res.data.body;
}
function saveData(
  data: any,
  onSuccess: any,
  onFailure: any,
  translationLanguge: any
) {
  if (translationLanguge) {
    APIKit.post("/topic/themes/translation/" + translationLanguge, data)
      .then(onSuccess)
      .catch(onFailure);
  } else {
    APIKit.post("/topic/themes", data).then(onSuccess).catch(onFailure);
  }
}
function TopicThemeDetails() {
  const language = useAppSelector(selectLanguage);
  const location = useLocation();
  const navigate = useNavigate();
  const [formTitle, setFormTitle] = useState("");
  const [title, setTitle] = useState("");
  const [subTitle, setSubTitle] = useState("");
  const [disableSaveButton, setDisableSaveButton] = useState(true);
  const [defaultTitle, setDefaultTitle] = useState("");
  const [itemData, setItemData] = useState<TopicTheme>();
  const [imageFile, setImageFile] = useState<any>();
  const [imageFileId, setImageFileId] = useState<any>();
  const [content, setContent] = useState<any>("");
  const [translationLanguge, setTranslationLanguge] = useState<any>();
  const [imageSettings, setImageSettings] = useState<ImageSettingDto[]>([]);
  const [models, setModels] = useState<any[]>([]);
  const [filteredModels, setFilteredModels] = useState<any[]>([]);
  const [filtering, setIsFiltering] = useState(true);
  const [loading, setLoading] = useState(true);
  const [modelIds, setModelIds] = useState([]);
  const [placeholder, setPlaceholder] = useState("");
  useEffect(() => {
    APIKit.get(`/model/`)
      .then(({ data }: any) => {
        let body = data.body.map((item: Model) => {
          return { id: item.id, value: item.defaultName };
        });
        setModels(body);
      })
      .catch(onFailure);
    APIKit.get("/image/setting/i_topic_theme")
      .then(({ data }: any) => {
        setImageSettings(data.body);
      })
      .catch(onFailure);

    if (location.state) {
      setTranslationLanguge(language.language);
      setDisableSaveButton(false);
      let obj = location.state as TopicTheme;
      setItemData(obj);
      setDefaultTitle(obj.defaultTitle);
      setFormTitle(obj.defaultTitle);
      setTitle(obj.title ? obj.title : "");
      setSubTitle(obj.subTitle ? obj.subTitle : "");
      setImageFileId(obj.imageFileId ? obj.imageFileId : null);
      setContent(obj.content ? obj.content : "");

      if (obj.id) {
        APIKit.get(`/available/model/${obj.id}/${language.language}`)
          .then(({ data }: any) => {
            let body = data.body.map((item: any) => {
              return { id: item.modelId, value: item.modelName };
            });
            setModelIds(body);
            setLoading(false);
          })
          .catch(onFailure);
      }
      if (modelIds.length > 0) {
        modelIds.map((item: any) => {
          let filter = models.filter((value: any) => {
            return item.id === value.id;
          });
          let index = models.indexOf(filter[0]);
          models.splice(index, 1);
        });
        setFilteredModels(models);
        setIsFiltering(false);
      }
    }
  }, [filtering, loading]);

  const validateUpload = (e: any, Width: number, Height: number) => {
    let fileTypeIsCorrect =
      e.target.files[0].type === "image/png" ||
      e.target.files[0].type === "image/gif" ||
      e.target.files[0].type === "image/jpeg" ||
      e.target.files[0].type === "video/mp4" ||
      e.target.files[0].type === "video/mvk" ||
      e.target.files[0].type === "audio/mp3";

    if (
      e.target.files[0].type === "image/png" ||
      e.target.files[0].type === "image/gif" ||
      e.target.files[0].type === "image/jpeg"
    ) {
      var files = e.target.files;
      //   var height = 0;
      //   var width = 0;
      var _URL = window.URL || window.webkitURL;
      for (var i = 0; i < files.length; i++) {
        var img = new Image();
        img.onload = function () {
          // height = img.height;
          // width = img.width;
        };
        img.src = _URL.createObjectURL(files[i]);
      }
    }
    return fileTypeIsCorrect;
  };
  const upload = (e: any, fileTypeCode: string) => {
    if (e.target.files[0].size <= 50000000) {
      let file = e.target.files[0];
      let render = new FileReader();
      render.onload = function (e) {
        uploadAsync(file, fileTypeCode).then((res) => {
          setImageFileId(res);
        });
        setImageFile(e.target?.result);
      };
      render.readAsDataURL(file);
    } else {
      e.target.value = "";
      alert("please uploD LESS THAN 5MB");
    }
  };
  const onSuccess = ({ data }: any) => {
    navigate("/topic-theme");
  };
  const onFailure = (error: any) => {
    console.log(error.response);
  };
  const getTopicThemesByIdAndTranslationLanguage = async (
    id: any,
    lang: any
  ) => {
    APIKit.get(`/topic/themes/${id}/${lang}`)
      .then(({ data }): any => {
        setItemData(data.body);
        setFormTitle(data.body.defaultTitle);
        setDisableSaveButton(data.body.defaultTitle ? false : true);
        setDefaultTitle(data.body.defaultTitle ? data.body.defaultTitle : "");
        setTitle(data.body.title ? data.body.title : "");
        setSubTitle(data.body.subTitle ? data.body.subTitle : "");
        setImageFileId(data.body?.imageFileId);
        setContent(data.body?.content ? data.body?.content : "");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const handleSave = () => {
    const obj = {
      id: itemData ? itemData.id : null,
      defaultTitle: defaultTitle,
      title: title,
      subTitle: subTitle,
      content: content,
      imageFileId: imageFileId,
    };
    saveData(obj, onSuccess, onFailure, translationLanguge);
    if (itemData) {
      let modelIdList = modelIds.map((item: any) => {
        return item.id;
      });

      let obj = {
        topicThemeId: itemData.id,
        modelIdList: modelIdList,
      };
      APIKit.post("/available/model", obj)
        .then(({ data }: any) => {
        })
        .catch(onFailure);
    }
  };
  return (
    <>
      <div className="px-5 content-body">
        <div className="card">
          <div className="card-header">
            <div className="flex justify-start ">
              <div className="flex-shrink-0 w-16 h-16">
                <img
                  className="w-16 h-16 transition duration-200 ease-in transform rounded-full cursor-pointer sm:hover:scale-125 hover:z-50"
                  src={
                    !imageFileId
                      ? "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60"
                      : `http://localhost:9191/image/file/${imageFileId}`
                  }
                  alt=""
                />
              </div>
              <div className="mt-2 ml-4">
                <h4 className="card-title">
                  {formTitle ? formTitle : "Title"}
                </h4>
                <p className="py-1 text-sm">{subTitle}</p>
              </div>
            </div>
            <div className="flex">
              <div
                className="flex px-3 py-1 text-indigo-800 border-2 border-indigo-800 cursor-pointer rounded-3xl hover:bg-indigo-100"
                onClick={(e) => {
                  e.preventDefault();
                  navigate("/topic-theme");
                }}
              >
                <Backspace width={20} />
                <button className="pl-2"> Back</button>
              </div>
              <div
                className={
                  disableSaveButton
                    ? "px-3 py-1 mx-2 text-indigo-800 border-2 border-indigo-800 rounded-3xl bg-indigo-100"
                    : "px-3 py-1 mx-2 text-indigo-800 border-2 border-indigo-800 rounded-3xl hover:bg-indigo-100"
                }
              >
                <button disabled={disableSaveButton} onClick={handleSave}>
                  Save
                </button>
              </div>
            </div>
          </div>

          <div className="card-body">
            {/* Nav tabs */}
            <div className="default-tab">
              <ul className="nav nav-tabs" role="tablist">
                <li className="nav-item">
                  <a
                    className="nav-link active"
                    data-toggle="tab"
                    href="#default"
                  >
                    Default
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#basic-info">
                    Basic info
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#logo">
                    Logo
                  </a>
                </li>
              </ul>
              <div className="tab-content">
                <div
                  className="tab-pane fade active show"
                  id="default"
                  role="tabpanel"
                >
                  <div className="row">
                    <div className="pt-10 col-sm-10">
                      <div className="flex py-2">
                        <label
                          htmlFor="defualtName"
                          className="w-40 text-slate-600"
                        >
                          Default title{" "}
                        </label>
                        <input
                          type="text"
                          className="w-full mr-10 rounded-2xl border-slate-300"
                          value={defaultTitle}
                          onChange={(e) => {
                            e.preventDefault();
                            setFormTitle(e.target.value);
                            setDefaultTitle(e.target.value);

                            if (e.target.value === "") {
                              setDisableSaveButton(true);
                            } else {
                              setDisableSaveButton(false);
                            }
                          }}
                        />
                      </div>
                      <div className="flex py-2">
                        <label
                          htmlFor="modelategory"
                          className="w-40 text-slate-600"
                        >
                          Model{" "}
                        </label>
                        <div className="w-full mr-10">
                          <MultiselectComponent
                            items={filteredModels}
                            setItems={setFilteredModels}
                            selectedItems={modelIds}
                            setSelected={setModelIds}
                            placeholderData={placeholder}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade " id="basic-info" role="tabpanel">
                  <div className="row">
                    <div className="col-sm-9">
                      <div className="pt-10">
                        <div className="flex py-2">
                          <label
                            htmlFor="defualtName"
                            className="w-40 text-slate-600"
                          >
                            Title{" "}
                          </label>
                          <input
                            type="text"
                            className="w-full mr-10 rounded-2xl border-slate-300"
                            value={title}
                            onChange={(e) => {
                              setTitle(e.target.value);
                            }}
                          />
                        </div>
                        <div className="flex py-2">
                          <label
                            htmlFor="defualtName"
                            className="w-40 text-slate-600"
                          >
                            SubTilte{" "}
                          </label>
                          <input
                            type="text"
                            className="w-full mr-10 rounded-2xl border-slate-300"
                            value={subTitle}
                            onChange={(e) => {
                              setSubTitle(e.target.value);
                            }}
                          />
                        </div>
                        <div className="pt-4">
                          <label
                            htmlFor="defualtName"
                            className="w-40 text-slate-600"
                          >
                            Content
                          </label>
                          <div className="pt-2">
                            <textarea
                              value={content}
                              className="w-full border-slate-300 rounded-xl"
                              name="content"
                              id="content"
                              rows={10}
                              onChange={(e) => {
                                setContent(e.target.value);
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-3">
                      <fieldset className="form-group">
                        <div className="row pt-10">
                          <div className="col-sm-9">
                            <label className="col-form-label  pt-0">
                              Language
                            </label>
                            <div className="form-check">
                              {language.language === "uz" ? (
                                <input
                                  className="form-check-input cursor-pointer"
                                  id="lang-radio"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="uz"
                                  defaultChecked={true}
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicThemesByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              ) : (
                                <input
                                  className="form-check-input cursor-pointer"
                                  id="lang-radio"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="uz"
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicThemesByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              )}
                              <label className="form-check-label">Uzbek</label>
                            </div>
                            <div className="form-check cursor-pointer">
                              {language.language === "ru" ? (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="ru"
                                  defaultChecked={true}
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicThemesByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              ) : (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="ru"
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicThemesByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              )}
                              <label className="form-check-label">
                                Russian
                              </label>
                            </div>
                            <div className="form-check disabled">
                              {language.language === "en" ? (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="en"
                                  defaultChecked={true}
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicThemesByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              ) : (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="en"
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicThemesByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              )}
                              <label className="form-check-label">
                                English
                              </label>
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade" id="logo">
                  <div className="pt-4">
                    {/* <div className="flex"> */}
                    {imageSettings.map(
                      (item: ImageSettingDto, index: number) => {
                        return (
                          <div key={index} className="flex p-1">
                            <div className="px-2">
                              <label className="block px-2 text-sm font-medium text-gray-700">
                                {item.fieldDisplayName}
                              </label>
                              <div className="flex justify-center px-6 pt-5 pb-6 mt-1 border-2 border-gray-300 border-dashed rounded-md">
                                <div className="space-y-1 text-center">
                                  <svg
                                    className="w-12 h-12 mx-auto text-gray-400"
                                    stroke="currentColor"
                                    fill="none"
                                    viewBox="0 0 48 48"
                                    aria-hidden="true"
                                  >
                                    <path
                                      d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                      strokeWidth={2}
                                      strokeLinecap="round"
                                      strokeLinejoin="round"
                                    />
                                  </svg>
                                  <div className="flex text-sm text-gray-600">
                                    <label
                                      htmlFor={`file-upload${index}`}
                                      className="relative font-medium text-indigo-600 bg-white rounded-md cursor-pointer hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                                    >
                                      <span>Upload a file</span>
                                      <input
                                        id={`file-upload${index}`}
                                        name="originalLogo"
                                        type="file"
                                        className="sr-only"
                                        onChange={(e) => {
                                          e.preventDefault();
                                          let file = e.target.files;
                                          console.log(file?.length);

                                          if (
                                            file?.length === 1 &&
                                            validateUpload(
                                              e,
                                              item.width,
                                              item.height
                                            )
                                          ) {
                                            console.log("uploadig ...");
                                            upload(e, item.fileTypeCode);
                                          } else {
                                            alert(
                                              `Please upload only image and with ${item.sizes} size`
                                            );
                                          }
                                        }}
                                        accept="image/jpeg, image/gif, image/png, video/mp4, video/mvk, video/avi"
                                      />
                                    </label>
                                    <p className="pl-1">or drag and drop</p>
                                  </div>
                                  <p className="text-xs text-gray-500">
                                    {item.fileTypeCode} up to 5MB
                                  </p>
                                  <p className="text-xs text-gray-500">
                                    Image size must be {" " + item.sizes}
                                  </p>
                                </div>
                              </div>
                            </div>
                            <div className="p-4 ">
                              {imageFileId ? (
                                <img
                                  key={index}
                                  src={
                                    imageFile
                                      ? imageFile
                                      : `http://localhost:9191/image/file/${imageFileId}`
                                  }
                                  alt=""
                                  width={280}
                                  height={190}
                                />
                              ) : (
                                <></>
                              )}
                            </div>
                          </div>
                        );
                      }
                    )}
                    {/* </div> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default TopicThemeDetails;
