export interface KeySetting {
  id: number;
  displayName: string;
  value: string;
}
