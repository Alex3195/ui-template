export interface BrandResponse {
    id: null | number;
    masterCode: null | string;
    defaultName: string;
    name: null | string;
    description: null | string;
    content: null | string;
    displayLogoId: null | number;
    originalLogoId: null | number;
}