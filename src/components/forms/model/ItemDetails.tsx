import { Backspace } from "@material-ui/icons";
import React, { Fragment, useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { useAppSelector } from "../../../app/hooks";
import { selectLanguage } from "../../../features/lang/languageSlice";
import { APIKit } from "../../../request/requests";
import { ModelCategory } from "../../../types/ModelCategory";
import { BrandResponse } from "../../../types/Brand";
import { Model } from "../../../types/Model";
import AttributeTable from "../../table/attribute/AttributeTable";
import { PlusCircleIcon, XIcon } from "@heroicons/react/solid";
import { Dialog, Transition } from "@headlessui/react";
import { ModelAttribute } from "../../../types/ModelAttribute";

function ItemDetails({
  item,
  setItem,
  subTitle,
  setTranslationLanguage,
  translationLanguage,
}: any) {
  const navigate = useNavigate();
  const language = useAppSelector(selectLanguage);
  const [disableSaveButton, setDisableSaveButton] = useState(true);
  const [modalStateOpen, setModalStateOpen] = useState(false);
  const [formTitle, setFormTitle] = useState("Title");
  const [defaultName, setDefaultName] = useState("");
  const [name, setName] = useState<any>("");
  const [description, setDescription] = useState<any>("");
  const [content, setContent] = useState<any>("");
  const [modelCategoryId, setModelCategoryId] = useState(0);
  const [brandId, setBrandId] = useState(0);
  const [modelCategories, setModelCategories] = useState<ModelCategory[]>([]);
  const [brands, setBrands] = useState<BrandResponse[]>([]);
  const [attributes, setAttributes] = useState<ModelAttribute[]>([]);
  const [selectedAttributes, setSelectedAttributes] = useState<
    ModelAttribute[]
  >([]);
  const [values, setValues] = useState();
  useEffect(() => {
    const fetchModelCategories = async () => {
      return await APIKit.get("/model/category");
    };
    const fetchBrands = async () => {
      return await APIKit.get("/brand");
    };

    fetchBrands().then(({ data }: any) => {
      setBrands(data.body);
    });
    fetchModelCategories().then(({ data }: any) => {
      setModelCategories(data.body);
    });
    APIKit.get(`/model/attribute`)
      .then(({ data }: any) => {
        setAttributes(data.body);
      })
      .catch((error) => {
        console.log(error.response);
      });
    if (item) {
      console.log(item);

      setDisableSaveButton(false);
      setFormTitle(item.defaultName ? item.defaultName : "");
      setDefaultName(item.defaultName ? item.defaultName : "");
      setName(item.name ? item.name : "");
      setDescription(item.description ? item.description : "");
      setContent(item.content ? item.content : "");
      setBrandId(item.brandId ? item.brandId : 0);
      setModelCategoryId(item.categoryId ? item.categoryId : 0);

      APIKit.get(`/model/attributes/${item.id}/${language.language}`).then(
        ({ data }: any) => {
          setSelectedAttributes(data.body);
        }
      );
    }
  }, [item]);
  const getModelAttributes = () => {
    setModalStateOpen(true);
  };
  const handleSave = (e: React.MouseEvent) => {
    e.preventDefault();
    let modelItem = {
      id: item ? item.id : 0,
      defaultName: defaultName,
      name: name,
      brandId: brandId,
      categoryId: modelCategoryId,
      content: content,
      description: description,
    } as Model;
    if (!item) {
      APIKit.post("/model", modelItem).then(({ data }: any) => {
        navigate("/model");
      });
    } else {
      APIKit.post("/model/translation/" + translationLanguage, modelItem).then(
        ({ data }: any) => {
          console.log("====================================");
          console.log(data.body);
          console.log("====================================");
          navigate("/model");
        }
      );
    }
    console.log(modelItem, translationLanguage);
  };
  return (
    <>
      {/* Dialog form */}
      <div>
        <Transition.Root show={modalStateOpen} as={Fragment}>
          <Dialog
            as="div"
            className="fixed inset-0 z-10 overflow-y-auto "
            onClose={setModalStateOpen}
          >
            <div
              className="flex min-h-screen text-center border-b md:block md:px-2 lg:px-4 rounded-2xl"
              style={{ fontSize: 0 }}
            >
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Dialog.Overlay className="fixed inset-0 hidden transition-opacity bg-gray-500 bg-opacity-75 md:block" />
              </Transition.Child>

              {/* This element is to trick the browser into centering the modal contents. */}
              <span
                className="hidden md:inline-block md:align-middle md:h-screen"
                aria-hidden="true"
              >
                &#8203;
              </span>
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
                enterTo="opacity-100 translate-y-0 md:scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 translate-y-0 md:scale-100"
                leaveTo="opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
              >
                <div className="flex w-full text-base text-left transition transform md:inline-block md:max-w-2xl md:px-4 md:my-8 md:align-middle lg:max-w-4xl ">
                  <div className="relative flex items-center w-full px-4 pb-8 overflow-hidden bg-white border-b shadow-2xl pt-14 sm:px-6 sm:pt-8 md:p-6 lg:p-8 rounded-3xl">
                    <button
                      type="button"
                      className="absolute pb-5 text-gray-400 top-4 right-4 hover:text-gray-500 sm:top-8 sm:right-6 md:top-6 md:right-6 lg:top-8 lg:right-8"
                      onClick={() => setModalStateOpen(false)}
                    >
                      <span className="sr-only">Close</span>
                      <XIcon className="w-6 h-6" aria-hidden="true" />
                    </button>

                    <div className="grid items-start w-full">
                      <>
                        <div className="card mt-10">
                          <div className="card-header">
                            <h5 className="card-title">Attributes</h5>
                          </div>
                          <div className="card-body">
                            <table className="table table-bordered table-striped">
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Type</th>
                                </tr>
                              </thead>
                              <tbody>
                                {attributes.map((item: ModelAttribute) => {
                                  return (
                                    <tr
                                      key={item.id}
                                      className="cursor-pointer"
                                      onClick={(e) => {
                                        e.preventDefault();
                                        setSelectedAttributes([
                                          ...selectedAttributes,
                                          item,
                                        ]);
                                        attributes.splice(
                                          attributes.indexOf(item),
                                          1
                                        );
                                      }}
                                    >
                                      <td>
                                        {item.displayName
                                          ? item.displayName
                                          : item.defaultDisplayName}
                                      </td>
                                      <td>{item.attributeType}</td>
                                    </tr>
                                  );
                                })}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </>
                    </div>
                  </div>
                </div>
              </Transition.Child>
            </div>
          </Dialog>
        </Transition.Root>
      </div>
      <div className="px-5 content-body">
        <div className="card">
          <div className="card-header">
            <div className="flex justify-start ">
              <div className="mt-2 ml-4">
                <h4 className="card-title">
                  {formTitle ? formTitle : "Title"}
                </h4>
                <p className="py-1 text-sm">{subTitle}</p>
              </div>
            </div>
            <div className="flex">
              <div
                className="flex px-3 py-1 btn btn-outline-primary"
                onClick={(e) => {
                  e.preventDefault();
                  navigate("/model");
                }}
              >
                <Backspace width={20} />
                <button className="pl-2"> Back</button>
              </div>
              <div className="px-3 py-1 mx-2 btn btn-outline-primary">
                <button disabled={disableSaveButton} onClick={handleSave}>
                  Save
                </button>
              </div>
            </div>
          </div>

          <div className="card-body">
            {/* Nav tabs */}
            <div className="default-tab">
              <ul className="nav nav-tabs" role="tablist">
                <li className="nav-item">
                  <a
                    className="nav-link active"
                    data-toggle="tab"
                    href="#default"
                  >
                    Default
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#basic-info">
                    Basic info
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#attributes">
                    Attribute
                  </a>
                </li>
              </ul>
              <div className="tab-content">
                <div
                  className="tab-pane fade active show"
                  id="default"
                  role="tabpanel"
                >
                  <div className="row">
                    <div className="pt-10 col-sm-10">
                      <div className="flex py-2">
                        <label
                          htmlFor="defaultName"
                          className="w-40 text-slate-600"
                        >
                          Default name{" "}
                        </label>
                        <input
                          type="text"
                          className="w-full mr-10 rounded-2xl border-slate-300"
                          value={defaultName}
                          onChange={(e) => {
                            e.preventDefault();
                            setFormTitle(e.target.value);
                            setDefaultName(e.target.value);

                            if (e.target.value === "") {
                              setDisableSaveButton(true);
                            } else {
                              setDisableSaveButton(false);
                            }
                          }}
                        />
                      </div>
                      {modelCategories ? (
                        <div className="flex py-2">
                          <label
                            htmlFor="modelCategory"
                            className="w-40 text-slate-600"
                          >
                            Category{" "}
                          </label>
                          <select
                            id="modelCategory"
                            value={modelCategoryId}
                            onChange={(e) => {
                              e.preventDefault();
                              setModelCategoryId(parseInt(e.target.value));
                            }}
                            className="w-full mr-10 rounded-2xl border-slate-300"
                          >
                            <option value={0}>Select category</option>
                            {modelCategories.map(
                              (parentItem: ModelCategory, index: number) => {
                                return (
                                  <option key={index} value={parentItem.id}>
                                    {parentItem.defaultName}
                                  </option>
                                );
                              }
                            )}
                          </select>
                        </div>
                      ) : (
                        <></>
                      )}
                      {brands ? (
                        <div className="flex py-2">
                          <label
                            htmlFor="brand"
                            className="w-40 text-slate-600"
                          >
                            Brand{" "}
                          </label>
                          <select
                            id="brand"
                            value={brandId}
                            onChange={(e) => {
                              e.preventDefault();
                              setBrandId(parseInt(e.target.value));
                            }}
                            className="w-full mr-10 rounded-2xl border-slate-300"
                          >
                            <option value={0}>Select brand</option>
                            {brands.map(
                              (parentItem: BrandResponse, index: number) => {
                                return (
                                  <option
                                    key={index}
                                    value={parentItem.id?.toString()}
                                  >
                                    {parentItem.defaultName}
                                  </option>
                                );
                              }
                            )}
                          </select>
                        </div>
                      ) : (
                        <></>
                      )}
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade " id="basic-info" role="tabpanel">
                  <div className="row">
                    <div className="col-sm-9">
                      <div className="pt-10">
                        <div className="flex py-2">
                          <label
                            htmlFor="defaultName"
                            className="w-40 text-slate-600"
                          >
                            Name{" "}
                          </label>
                          <input
                            type="text"
                            className="w-full mr-10 rounded-2xl border-slate-300"
                            value={name}
                            onChange={(e) => {
                              setName(e.target.value);
                            }}
                          />
                        </div>
                        <div className="flex py-2">
                          <label
                            htmlFor="defaultName"
                            className="w-40 text-slate-600"
                          >
                            Description{" "}
                          </label>
                          <input
                            type="text"
                            className="w-full mr-10 rounded-2xl border-slate-300"
                            value={description}
                            onChange={(e) => {
                              setDescription(e.target.value);
                            }}
                          />
                        </div>
                      </div>
                      <div className="pt-4">
                        <h4>Content</h4>
                        <div className="pt-2">
                          <textarea
                            value={content}
                            className="w-full border-slate-300 rounded-xl"
                            name="content"
                            id="content"
                            rows={10}
                            onChange={(e) => {
                              setContent(e.target.value);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-3">
                      <fieldset className="form-group">
                        <div className="row pt-10">
                          <div className="col-sm-9">
                            <label className="col-form-label  pt-0">
                              Language
                            </label>
                            <div className="form-check">
                              {language.language === "uz" ? (
                                <input
                                  className="form-check-input cursor-pointer"
                                  id="lang-radio"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="uz"
                                  defaultChecked={true}
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguage(e.target.value);
                                  }}
                                />
                              ) : (
                                <input
                                  className="form-check-input cursor-pointer"
                                  id="lang-radio"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="uz"
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguage(e.target.value);
                                  }}
                                />
                              )}
                              <label className="form-check-label">Uzbek</label>
                            </div>
                            <div className="form-check cursor-pointer">
                              {language.language === "ru" ? (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="ru"
                                  defaultChecked={true}
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguage(e.target.value);
                                  }}
                                />
                              ) : (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="ru"
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguage(e.target.value);
                                  }}
                                />
                              )}
                              <label className="form-check-label">
                                Russian
                              </label>
                            </div>
                            <div className="form-check disabled">
                              {language.language === "en" ? (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="en"
                                  defaultChecked={true}
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguage(e.target.value);
                                  }}
                                />
                              ) : (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="en"
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguage(e.target.value);
                                  }}
                                />
                              )}
                              <label className="form-check-label">
                                English
                              </label>
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade " id="attributes" role="tabpanel">
                  <div className="flex justify-content-end mt-4">
                    <button
                      className="btn btn-sm btn-outline-primary"
                      onClick={(e) => {
                        e.preventDefault();
                        getModelAttributes();
                      }}
                    >
                      <div className="flex">
                        <PlusCircleIcon
                          width={20}
                          height={20}
                          className="mr-2"
                        />{" "}
                        <span className="text-center">Add attribute</span>
                      </div>
                    </button>
                  </div>
                  {selectedAttributes.length > 0 ? (
                    <AttributeTable
                      modelId={item.id}
                      items={selectedAttributes}
                      setItems={setSelectedAttributes}
                    />
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ItemDetails;
