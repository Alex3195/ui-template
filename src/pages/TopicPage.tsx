import React from "react";
import TopicTable from "../components/table/topic/TopicTable";

function TopicPage() {
  return (
    <>
      <TopicTable />
    </>
  );
}

export default TopicPage;
