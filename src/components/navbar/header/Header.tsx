/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { Translate } from "@material-ui/icons";
import React, { useEffect } from "react";
import { useNavigate } from "react-router";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import {
  setLanguage,
  selectLanguage,
} from "../../../features/lang/languageSlice";
import { logout, selectUser } from "../../../features/user/userSlice";

function Header() {
  const dispatch = useAppDispatch();
  const user = useAppSelector(selectUser);
  const language = useAppSelector(selectLanguage);
  const navigate = useNavigate();
  return (
    <div className="header">
      <div className="header-content">
        <nav className="navbar navbar-expand">
          <div className="collapse navbar-collapse justify-content-between">
            <div className="header-left">
              <div className="dashboard_bar">Dashboard</div>
            </div>
            <ul className="navbar-nav header-right">
              <li className="nav-item">
                <div className="input-group search-area d-xl-inline-flex d-none">
                  <div className="input-group-append">
                    <span className="input-group-text">
                      <a href="#/">
                        <i className="flaticon-381-search-2" />
                      </a>
                    </span>
                  </div>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search here..."
                  />
                </div>
              </li>
              <li className="nav-item dropdown notification_dropdown">
                <a
                  className="nav-link ai-icon"
                  href="#/"
                  role="button"
                  data-toggle="dropdown"
                >
                  <Translate />
                </a>
                <div className="rounded dropdown-menu dropdown-menu-right">
                  <div
                    id="DZ_W_Notification1"
                    className="p-3 widget-media dz-scroll "
                  >
                    <ul className="timeline">
                      <li>
                        <div
                          className={
                            language.language === "en"
                              ? "timeline-panel cursor-pointer rounded-lg p-2 bg-blue-100"
                              : "timeline-panel cursor-pointer rounded-lg p-2"
                          }
                          onClick={(e) => {
                            dispatch(setLanguage("en"));
                          }}
                        >
                          <div className="mr-2 media media-info">ENG</div>
                          <div className="media-body">
                            <h6 className="mb-1">English</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div
                          className={
                            language.language === "ru"
                              ? "timeline-panel cursor-pointer rounded-lg p-2 bg-blue-100"
                              : "timeline-panel cursor-pointer rounded-lg p-2"
                          }
                          onClick={(e) => {
                            dispatch(setLanguage("ru"));
                          }}
                        >
                          <div className="mr-2 media media-success">RU</div>
                          <div className="media-body">
                            <h6 className="mb-1">Russian</h6>
                          </div>
                        </div>
                      </li>

                      <li>
                        <div
                          className={
                            language.language === "uz"
                              ? "timeline-panel cursor-pointer rounded-lg p-2 bg-blue-100"
                              : "timeline-panel cursor-pointer rounded-lg p-2"
                          }
                          onClick={(e) => {
                            dispatch(setLanguage("uz"));
                          }}
                        >
                          <div className="mr-2 media media-danger">UZ</div>
                          <div className="media-body">
                            <h6 className="mb-1">Uzbek</h6>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
              <li className="nav-item dropdown header-profile">
                <a
                  className="nav-link"
                  href="#/"
                  role="button"
                  data-toggle="dropdown"
                >
                  <img
                    src={
                      user.user?.image
                        ? `http://localhost:9191/image/file/${user.user.image}`
                        : "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60"
                    }
                    width={20}
                    alt=""
                    className="sm:hover:scale-125 hover:z-50"
                  />
                  <div className="header-info">
                    <span className="text-black ">
                      <strong>{user.user?.fullName}</strong>
                    </span>
                    <p className="mb-0 fs-12">{user.user?.role}</p>
                  </div>
                </a>
                <div className="dropdown-menu dropdown-menu-right">
                  <div
                    className="cursor-pointer dropdown-item ai-icon"
                    onClick={(e) => {
                      console.log("profile");
                    }}
                  >
                    <div className="flex">
                      <svg
                        id="icon-user1"
                        xmlns="http://www.w3.org/2000/svg"
                        className="text-primary"
                        width={18}
                        height={18}
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth={2}
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      >
                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" />
                        <circle cx={12} cy={7} r={4} />
                      </svg>
                      <span className="ml-2">Profile </span>
                    </div>
                  </div>

                  <div
                    className="cursor-pointer dropdown-item ai-icon"
                    onClick={(e) => {
                      dispatch(logout());
                      navigate("/");
                    }}
                  >
                    <div className="flex">
                      <svg
                        id="icon-logout"
                        xmlns="http://www.w3.org/2000/svg"
                        className="text-danger"
                        width={18}
                        height={18}
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth={2}
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      >
                        <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4" />
                        <polyline points="16 17 21 12 16 7" />
                        <line x1={21} y1={12} x2={9} y2={12} />
                      </svg>
                      <span className="ml-2">Logout </span>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  );
}

export default Header;
