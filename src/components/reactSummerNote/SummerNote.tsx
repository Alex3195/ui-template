import React, { useEffect } from "react";

function SummerNote() {
 
  return (
    <div className="content-body">
      <div className="container-fluid">
        <div className="page-titles">
          <ol className="breadcrumb">
            <li className="breadcrumb-item">
              <a href="javascript:void(0)">Form</a>
            </li>
            <li className="breadcrumb-item active">
              <a href="javascript:void(0)">Summernote</a>
            </li>
          </ol>
        </div>
        <div className="row">
          <div className="col-xl-12 col-xxl-12">
            <div className="card">
              <div className="card-header">
                <h4 className="card-title">Summernote Editor</h4>
              </div>
              <div className="card-body">
                <div className="summernote"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SummerNote;
