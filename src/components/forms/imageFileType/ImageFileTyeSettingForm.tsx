import { useEffect, useState } from "react";
import { ImageType } from "../../../features/user/imageType";
import { APIKit } from "../../../request/requests";

function ImageFileTyeSettingForm({
  item,
  setOpen,
  imageTypes,
  setImageTypes,
}: any) {
  let initState: ImageType;
  initState = {
    id: null,
    typeCode: "",
    displayName: "",
  };
  const [showMsg, setShowMsg] = useState(false);
  const [imageTypeItem, setImageTypeItem] = useState<ImageType>(initState);
  const handleSubmit = (e: React.MouseEvent) => {
    e.preventDefault();
    if (item) {
      if (imageTypeItem.typeCode) {
        if (imageTypeItem.displayName === "") {
          setImageTypeItem({
            ...imageTypeItem,
            displayName: imageTypeItem.typeCode,
          });
        }
        APIKit.put("/type/images/", imageTypeItem)
          .then(onSuccess)
          .catch(onFailure);
      } else {
        setShowMsg(true);
      }
    } else {
      if (imageTypeItem.typeCode) {
        if (imageTypeItem.displayName === "" || !imageTypeItem.displayName) {
          setImageTypeItem({
            ...imageTypeItem,
            displayName: imageTypeItem.typeCode,
          });
        }
        APIKit.post("/type/images/", imageTypeItem)
          .then(onSuccess)
          .catch(onFailure);
      } else {
        setShowMsg(true);
      }
    }
    console.log(imageTypeItem);
  };
  const onSuccess = ({ data }: any) => {
    if (item) {
      const newList = imageTypes.map((mapItem: any) => {
        if (item.id === mapItem.id) {
          return data.body;
        }

        return mapItem;
      });

      setImageTypes(newList);
    } else {
      setImageTypes([...imageTypes, data.body]);
    }
    setOpen(false);
  };

  const onFailure = (errors: any) => {
    console.log(errors.response);
    setOpen(false);
  };
  useEffect(() => {
    if (item) {
      setImageTypeItem(item);
    }
  }, []);
  return (
    <>
      <div className="col-xl-12 col-lg-12 lg:mt-5">
        <div className="card">
          <div className="card-header">
            <h4 className="card-title">Image type</h4>
          </div>
          <div className="card-body bg-slate-50 ">
            <div className="basic-form min-w-fit">
              <form>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">
                    Display name
                  </label>
                  <div className="col-sm-9">
                    <input
                      value={imageTypeItem.displayName}
                      onChange={(e) => {
                        e.preventDefault();
                        setImageTypeItem({
                          ...imageTypeItem,
                          displayName: e.target.value,
                        });
                      }}
                      type="text"
                      className="text-black form-control"
                      placeholder="Display name"
                    />
                  </div>
                </div>
                {showMsg ? (
                  <div className="text-red-400">Please enter image type!!!</div>
                ) : (
                  ""
                )}
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Image type</label>
                  <div className="col-sm-9">
                    <input
                      required
                      value={imageTypeItem.typeCode}
                      onChange={(e) => {
                        e.preventDefault();
                        if (e.target.value === "") {
                          setShowMsg(true);
                        } else {
                          setShowMsg(false);
                        }
                        setImageTypeItem({
                          ...imageTypeItem,
                          typeCode: e.target.value,
                        });
                      }}
                      type="text"
                      className="text-black form-control"
                      placeholder="png, jpeg or jpg"
                    />
                  </div>
                </div>
                <div className="flex justify-end">
                  <button
                    onClick={handleSubmit}
                    type="submit"
                    className="p-2 pl-4 pr-4 text-xl text-white bg-blue-500 border-2 rounded-xl hover:bg-blue-600 "
                  >
                    Save
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ImageFileTyeSettingForm;
