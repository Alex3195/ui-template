import React, { useEffect, useState } from "react";
import { APIKit } from "../../../request/requests";
import { TopicCategory } from "../../../types/TopicCategory";
import MainTable from "../MainTable";
import { useAppSelector } from '../../../app/hooks';
import { selectLanguage } from '../../../features/lang/languageSlice';
const headerTitle = ["Title", "Subtitle", "parent", "action"];
function TopicCategroyTable() {
  const [data, setData] = useState<TopicCategory[]>([]);
  const [loading, setLoading] = useState(true);
  const language = useAppSelector(selectLanguage)
  useEffect(() => {
    APIKit.get("/topic/categories").then(onSuccess);
  }, [language.language]);
  const onSuccess = ({ data }: any) => {
    data.body.sort(function (a: any, b: any) {
      let x = a.defaultTitle.toLowerCase();
      let y = b.defaultTitle.toLowerCase();
      if (x < y) {
        return -1;
      }
      if (x > y) {
        return 1;
      }
      return 0;
    });
    setData(data.body);
    setLoading(false);
  };
  return (
    <div>
      {loading ? (
        <></>
      ) : (
        <MainTable
          header={"Topic category"}
          headerTitles={headerTitle}
          addFormUrl={"/topic-category-item"}
          editFormUrl={"/topic-category-item"}
          data={data}
        />
      )}
    </div>
  );
}

export default TopicCategroyTable;
