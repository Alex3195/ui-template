import { Menu, Transition } from "@headlessui/react";
import {
  ChevronDownIcon,
  LinkIcon,
  PencilAltIcon,
  PlusCircleIcon,
  TrashIcon,
} from "@heroicons/react/solid";
import { ArrowLeft, ArrowRight } from "@material-ui/icons";
import { Fragment, useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { APIKit } from "../../request/requests";
import { UserRespone } from "../../types/userResponseType";
import React from "react";
function classNames(...classes: any) {
  return classes.filter(Boolean).join(" ");
}
function previousPage(pageNum: any, setPageNum: any) {
  if (pageNum > 1) {
    setPageNum(pageNum - 1);
  }
}
function nextPage(pageNum: any, setPageNum: any, maxPage: number) {
  if (pageNum < maxPage) {
    setPageNum(pageNum + 1);
  }
}
async function fetchUsers(onSuccess: any, onFailure: any) {
  APIKit.get("/users/").then(onSuccess).catch(onFailure);
}
async function dropUser(id: any, onFailure: any) {
  APIKit.delete(`/users/delete/${id}`)
    .then((res) => {
      console.log(res.data);
    })
    .catch(onFailure);
}
function searchFromArray(data: any, value: any) {
  return data.filter(
    (item: any) =>
      item.fullName.toLowerCase().indexOf(value.toLowerCase()) !== -1
  );
}

export default function UserTable() {
  const [users, setUsers] = useState<UserRespone[]>([]);
  const [paging, setPaging] = useState<any>(5);
  const [pageNum, setPageNum] = useState<any>(1);
  const [searchData, setSearchData] = useState("");
  const [resultSerach, setResultSearch] = useState<UserRespone[]>([]);
  const navigate = useNavigate();
  const drop = (item: UserRespone) => {
    console.log("Drop user");
    dropUser(item.id, onFailure);
    let index = users.indexOf(item);
    users.splice(index, 1);
  };
  useEffect(() => {
    fetchUsers(onSuccess, onFailure);
  }, []);

  const editForm = (item: UserRespone) => {
    navigate("/user-form", { state: item });
  };
  const addForm = () => {
    navigate("/user-form");
  };
  const onSuccess = ({ data }: any) => {
    let array = data.body;
    array.sort(function (a: any, b: any) {
      let x = a.fullName.toLowerCase();
      let y = b.fullName.toLowerCase();
      if (x < y) {
        return -1;
      }
      if (x > y) {
        return 1;
      }
      return 0;
    });   
    setUsers(array);
    setResultSearch(array);
    users.sort();
  };

  const onFailure = (errors: any) => {
    console.log(errors.response);
  };

  return (
    <>
      <div className="content-body">
        {/* buttons */}
        <div className="flex py-5 lg:items-center lg:justify-between lg:mx-16">
          <div className="flex-1 min-w-0">
            <h2 className="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl ">
              Users
            </h2>
          </div>
          <div className="flex mt-5 lg:mt-0 lg:ml-4">
            <span className="hidden sm:block">
              <button
                type="button"
                onClick={(e) => {
                  e.preventDefault();
                  addForm();
                }}
                className="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-blue-800 border border-gray-300 rounded-md shadow-sm hover:bg-blue-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                <PlusCircleIcon
                  className="w-5 h-5 mr-2 -ml-1 text-white"
                  aria-hidden="true"
                />
                Add
              </button>
            </span>

            <span className="hidden ml-3 sm:block">
              <button
                type="button"
                onClick={(e) => {
                  e.preventDefault();
                  console.log("___");
                }}
                className="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-blue-800 border border-gray-300 rounded-md shadow-sm hover:bg-blue-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                <LinkIcon
                  className="w-5 h-5 mr-2 -ml-1 text-white"
                  aria-hidden="true"
                />
                Another action
              </button>
            </span>

            {/* Dropdown */}
            <Menu as="span" className="relative ml-3 sm:hidden">
              <Menu.Button className="inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                More
                <ChevronDownIcon
                  className="w-5 h-5 ml-2 -mr-1 text-gray-500"
                  aria-hidden="true"
                />
              </Menu.Button>

              <Transition
                as={Fragment}
                enter="transition ease-out duration-200"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
              >
                <Menu.Items className="absolute right-0 w-48 py-1 mt-2 -mr-1 origin-top-right bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                  <Menu.Item>
                    {({ active }) => (
                      <a
                        href="/user-form"
                        className={classNames(
                          active ? "bg-gray-100" : "",
                          "block px-4 py-2 text-sm text-gray-700"
                        )}
                      >
                        Add
                      </a>
                    )}
                  </Menu.Item>
                </Menu.Items>
              </Transition>
            </Menu>
          </div>
        </div>
        {/* table */}
        <div className="flex flex-col">
          <div className="-my-2 overflow-x-auto sm:mx-6 lg:mx-8">
            <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
              <div className="border-b border-gray-200 shadow scrollbar-hide sm:rounded-lg">
                <div className="flex justify-end pr-10 pt-4">
                  <div className="input-group search-area d-xl-inline-flex d-none">
                    <div className="input-group-append">
                      <span className="input-group-text">
                        <div className="cursor-pointer">
                          <i className="flaticon-381-search-2" />
                        </div>
                      </span>
                    </div>
                    <input
                      type="text"
                      className="form-control"
                      onChange={(e) => {
                        if (e.target.value === "") {
                          setResultSearch(users);
                        } else {
                          setSearchData(e.target.value);
                          let result = searchFromArray(users, searchData);
                          setResultSearch(result);
                        }
                      }}
                      placeholder="search"
                    />
                  </div>
                </div>
                <table className="min-w-full overflow-y-auto divide-y divide-gray-200">
                  <thead className="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        className="text-xs font-medium tracking-wider text-left text-gray-500 uppercase lg:px-6 lg:py-3"
                      >
                        Name
                      </th>
                      <th
                        scope="col"
                        className="text-xs font-medium tracking-wider text-left text-gray-500 uppercase lg:px-6 lg:py-3"
                      >
                        Title
                      </th>
                      <th
                        scope="col"
                        className="text-xs font-medium tracking-wider text-left text-gray-500 uppercase lg:px-6 lg:py-3"
                      >
                        Status
                      </th>
                      <th
                        scope="col"
                        className="text-xs font-medium tracking-wider text-left text-gray-500 uppercase lg:px-6 lg:py-3"
                      >
                        Role
                      </th>
                      <th
                        scope="col"
                        className="text-xs font-medium tracking-wider text-center text-gray-500 uppercase lg:px-6 lg:py-3"
                      >
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {resultSerach.map((person, index) => {
                      if (
                        index < pageNum * paging &&
                        index >= (pageNum - 1) * paging
                      )
                        return (
                          <tr
                            key={index}
                            className={
                              index % 2 === 1 ? "bg-slate-100" : "bg-slate-50"
                            }
                          >
                            <td className=" lg:px-6 lg:py-4 whitespace-nowrap">
                              <div className="flex items-center">
                                <div className="flex-shrink-0 w-16 h-16">
                                  <img
                                    className="w-16 h-16 transition duration-200 ease-in transform rounded-full cursor-pointer sm:hover:scale-125 hover:z-50"
                                    src={
                                      person.imageId
                                        ? "http://localhost:9191/image/file/"+person.imageId
                                        : "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60"
                                    }
                                    alt=""
                                  />
                                </div>
                                <div className="ml-4">
                                  <div className="text-sm font-medium text-gray-900">
                                    {person.fullName}
                                  </div>
                                  <div className="text-sm text-gray-500">
                                    {person.username}
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td className=" lg:py-4 lg:px-6 whitespace-nowrap">
                              <div className="text-sm text-gray-900 ">
                                {person.username}
                              </div>
                              <div className="text-sm text-gray-500 ">
                                {person.role}
                              </div>
                            </td>
                            <td className=" lg:px-6 lg:py-4 whitespace-nowrap">
                              <span
                                className={classNames(
                                  person.isActive
                                    ? "inline-flex px-2 text-xs font-semibold leading-5 text-green-800 bg-green-100 rounded-full"
                                    : "inline-flex px-2 text-xs font-semibold leading-5 text-slate-500 bg-red-100 rounded-full"
                                )}
                              >
                                {person.isActive ? "Active" : "Passive"}
                              </span>
                            </td>
                            <td className="text-sm text-gray-500 lg:px-6 lg:py-4 whitespace-nowrap ">
                              {person.role}
                            </td>
                            <td className="flex justify-end px-6 py-4 text-sm font-medium text-rfirst-letter:ight whitespace-nowrap ">
                              <span className="hidden ml-3 sm:block">
                                <button
                                  type="button"
                                  onClick={(e) => {
                                    e.preventDefault();
                                    editForm(person);
                                  }}
                                  className="inline-flex items-center px-2 text-sm font-medium text-blue-300 border-2 border-blue-300 rounded-md shadow-sm lg:py-1 bg-slate-50 hover:bg-blue-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                >
                                  <PencilAltIcon
                                    className="w-5 h-5 mr-2 -ml-1 text-blue-300"
                                    aria-hidden="true"
                                  />
                                  Edit
                                </button>
                              </span>
                              <span className="hidden ml-3 sm:block">
                                <button
                                  type="button"
                                  onClick={(e) => {
                                    e.preventDefault();
                                    drop(person);
                                  }}
                                  className="inline-flex items-center px-2 text-sm font-medium text-blue-300 border-2 border-blue-300 rounded-md shadow-sm lg:py-1 bg-slate-50 hover:bg-blue-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                >
                                  <TrashIcon
                                    className="w-5 h-5 mr-2 -ml-1 text-blue-300"
                                    aria-hidden="true"
                                  />
                                  Drop
                                </button>
                              </span>
                            </td>
                          </tr>
                        );
                    })}
                  </tbody>
                </table>
                <div className="relative flex justify-end h-16 border-t-2">
                  <div className="px-2 pt-3">Rows per page:</div>
                  <div className="flex-row justify-end pt-2">
                    <select
                      className="border-gray-300 rounded-lg border-1"
                      value={paging}
                      onChange={(e) => {
                        setPaging(e.target.value);
                      }}
                      name="paging"
                      id="pageNums"
                    >
                      <option value={5}>5</option>
                      <option value={10}>10</option>
                      <option value={25}>25</option>
                      <option value={50}>50</option>
                      <option value={100}>100</option>
                    </select>
                  </div>
                  <div className="flex-row justify-end lg:py-3 lg:px-10">
                    {pageNum} of{" "}
                    {users.length % paging !== 0 && users.length / paging > 1
                      ? (users.length / paging + 1).toFixed()
                      : users.length / paging > 0.5
                      ? (users.length / paging).toFixed()
                      : (users.length / paging + 1).toFixed()}
                  </div>
                  <div className="flex-row justify-end lg:py-3 lg:px-10">
                    <ArrowLeft
                      onClick={(e) => {
                        e.preventDefault();
                        previousPage(pageNum, setPageNum);
                      }}
                      className="cursor-pointer"
                    />
                    <ArrowRight
                      onClick={(e) => {
                        e.preventDefault();
                        let maxPage =
                          users.length % paging === 1
                            ? users.length / paging + 1
                            : users.length / paging;
                        nextPage(pageNum, setPageNum, maxPage);
                      }}
                      className="cursor-pointer"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
