export interface Topic {
    id: number;
    title: string;
    defaultTitle: string;
    subTitle: string;
    content: string;
    topicThemeId: number;
    themeTitle:string;
}