import { Dialog, Menu, Transition } from "@headlessui/react";
import { ChevronDownIcon, PlusIcon, XIcon } from "@heroicons/react/solid";
import { Fragment, useEffect, useState } from "react";
import { APIKit } from "../../request/requests";
import ImageFileTyeSettingForm from "../forms/imageFileType/ImageFileTyeSettingForm";
import { ImageType } from "../../features/user/imageType";

function classNames(...classes: any) {
  return classes.filter(Boolean).join(" ");
}
function ImageTypeSettingsTable() {
  const [modalStateOpen, setModalStateOpen] = useState(false);
  const [imageTypes, setImageTypes] = useState<any[]>([]);
  const [imageTypeItem, setImageTypeItem] = useState<any>(null);
  const onSuccess = ({ data }: any) => {
    setImageTypes(data.body);
  };
  const onFailure = (errors: any) => {
    console.log("Imagesettings --> ", errors.response);
  };
  const onEdit = (item: any) => {
    setImageTypeItem(item);
  };
  const dropItem = (item: ImageType) => {
    APIKit.delete("/type/images/" + item.id)
      .then((res) => {
        imageTypes.splice(imageTypes.indexOf(item), 1);
        console.log(imageTypes);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  useEffect(() => {
    APIKit.get("/type/images/").then(onSuccess).catch(onFailure);
  }, []);
  return (
    <>
      {/* Dialog form */}
      <div>
        <Transition.Root show={modalStateOpen} as={Fragment}>
          <Dialog
            as="div"
            className="fixed inset-0 z-10 overflow-y-auto "
            onClose={setModalStateOpen}
          >
            <div
              className="flex min-h-screen text-center border-b md:block md:px-2 lg:px-4 rounded-2xl"
              style={{ fontSize: 0 }}
            >
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Dialog.Overlay className="fixed inset-0 hidden transition-opacity bg-gray-500 bg-opacity-75 md:block" />
              </Transition.Child>

              {/* This element is to trick the browser into centering the modal contents. */}
              <span
                className="hidden md:inline-block md:align-middle md:h-screen"
                aria-hidden="true"
              >
                &#8203;
              </span>
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
                enterTo="opacity-100 translate-y-0 md:scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 translate-y-0 md:scale-100"
                leaveTo="opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
              >
                <div className="flex w-full text-base text-left transition transform md:inline-block md:max-w-2xl md:px-4 md:my-8 md:align-middle lg:max-w-4xl ">
                  <div className="relative flex items-center w-full px-4 pb-8 overflow-hidden bg-white border-b shadow-2xl pt-14 sm:px-6 sm:pt-8 md:p-6 lg:p-8 rounded-3xl">
                    <button
                      type="button"
                      className="absolute pb-5 text-gray-400 top-4 right-4 hover:text-gray-500 sm:top-8 sm:right-6 md:top-6 md:right-6 lg:top-8 lg:right-8"
                      onClick={() => setModalStateOpen(false)}
                    >
                      <span className="sr-only">Close</span>
                      <XIcon className="w-6 h-6" aria-hidden="true" />
                    </button>

                    <div className="grid items-start w-full ">
                      <ImageFileTyeSettingForm
                        item={imageTypeItem}
                        setOpen={setModalStateOpen}
                        imageTypes={imageTypes}
                        setImageTypes={setImageTypes}
                      />
                    </div>
                  </div>
                </div>
              </Transition.Child>
            </div>
          </Dialog>
        </Transition.Root>
      </div>

      {/* buttons */}
      <div className="flex lg:items-center lg:justify-between lg:mx-16">
        <div className="flex mb-2">
          <span className="hidden sm:block">
            <button
              type="button"
              onClick={(e) => {
                e.preventDefault();
                setModalStateOpen(true);
              }}
              className="inline-flex w-16 h-16 items-center px-4 py-2 text-sm font-medium text-white bg-indigo-800 border-gray-300 rounded-md shadow-sm bg-ind hover:bg-indigo-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <PlusIcon className="w-5 h-5 text-white" aria-hidden="true" />
            </button>
          </span>

          {/* Dropdown */}
          <Menu as="span" className="relative ml-3 sm:hidden">
            <Menu.Button className="inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              More
              <ChevronDownIcon
                className="w-5 h-5 ml-2 -mr-1 text-gray-500"
                aria-hidden="true"
              />
            </Menu.Button>

            <Transition
              as={Fragment}
              enter="transition ease-out duration-200"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Menu.Items className="absolute right-0 w-48 py-1 mt-2 -mr-1 origin-top-right bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                <Menu.Item>
                  {({ active }) => (
                    <a
                      href="#edit-user"
                      className={classNames(
                        active ? "bg-gray-100" : "",
                        "block px-4 py-2 text-sm text-gray-700"
                      )}
                    >
                      Edit
                    </a>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <a
                      href="add-user"
                      className={classNames(
                        active ? "bg-gray-100" : "",
                        "block px-4 py-2 text-sm text-gray-700"
                      )}
                    >
                      Add
                    </a>
                  )}
                </Menu.Item>
              </Menu.Items>
            </Transition>
          </Menu>
        </div>
      </div>
      {/* table */}
      <div className="flex flex-col py-2">
        <div className="-my-2 overflow-x-auto sm:mx-6 lg:mx-8">
          <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
            <div className="border-b border-gray-200 shadow scrollbar-hide sm:rounded-lg">
              <table className="min-w-full overflow-y-auto divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Display name
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Type
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Actions
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {imageTypes? (
                    imageTypes.map((item: ImageType, index: number) => {
                      return (
                        <tr key={index}>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="flex items-center">
                              <div className="ml-4">
                                <div className="text-sm font-medium text-gray-900">
                                  {item.displayName}
                                </div>
                              </div>
                            </div>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="text-sm text-gray-900">
                              {item.typeCode}
                            </div>
                          </td>
                          <td className="flex justify-end px-6 py-4 text-sm font-medium text-rfirst-letter:ight whitespace-nowrap">
                            <div className="flex justify-start">
                              <a
                                href="#/"
                                className="mr-1 shadow btn btn-primary sharp"
                                onClick={(e) => {
                                  e.preventDefault();
                                  onEdit(item);
                                  setModalStateOpen(true);
                                }}
                              >
                                <i className="fa fa-pencil" />
                              </a>
                              <a
                                href="#/"
                                className="shadow btn btn-danger sharp"
                                onClick={(e) => {
                                  e.preventDefault();
                                  dropItem(item);
                                }}
                              >
                                <i className="fa fa-trash" />
                              </a>
                            </div>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={4}>
                        <p className="flex justify-center">There is no data</p>
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ImageTypeSettingsTable;
