// A mock function to mimic making an async request for data
import {User} from "../../types/user";

export function fetchUser(user: User) {
    return new Promise<{ data: User }>((resolve) =>
        setTimeout(() => resolve({data: user}), 500)
    );
}
