import { XIcon } from "@heroicons/react/solid";
import React,{ Fragment, useEffect, useState } from "react";
import { Transition, Dialog } from "@headlessui/react";
import SettingsKeyForm from "../../settingsKeyForm/SettingsKeyForm";
import { KeySetting } from "../../../types/KeySetting";
import { APIKit } from "../../../request/requests";
import { PlusIcon } from "@heroicons/react/outline";
import SettingTypeForm from "./SettingTypeForm";

function SettingTypes() {
  const [modalStateOpen, setModalStateOpen] = useState(false);
  const [settingTypes, setSettingTypes] = useState<KeySetting[]>([]);

  useEffect(() => {
    APIKit.get("/key")
      .then(({ data }: any) => {
        setSettingTypes(data.body);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [modalStateOpen]);
  return (
    <>
      {/* Dialog form */}
      <div>
        <Transition.Root show={modalStateOpen} as={Fragment}>
          <Dialog
            as="div"
            className="fixed inset-0 z-10 overflow-y-auto "
            onClose={setModalStateOpen}
          >
            <div
              className="flex min-h-screen text-center border-b md:block md:px-2 lg:px-4 rounded-2xl"
              style={{ fontSize: 0 }}
            >
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Dialog.Overlay className="fixed inset-0 hidden transition-opacity bg-gray-500 bg-opacity-75 md:block" />
              </Transition.Child>

              {/* This element is to trick the browser into centering the modal contents. */}
              <span
                className="hidden md:inline-block md:align-middle md:h-screen"
                aria-hidden="true"
              >
                &#8203;
              </span>
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
                enterTo="opacity-100 translate-y-0 md:scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 translate-y-0 md:scale-100"
                leaveTo="opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
              >
                <div className="flex w-full text-base text-left transition transform md:inline-block md:max-w-2xl md:px-4 md:my-8 md:align-middle lg:max-w-4xl ">
                  <div className="relative flex items-center w-full px-4 pb-8 overflow-hidden bg-white border-b shadow-2xl pt-14 sm:px-6 sm:pt-8 md:p-6 lg:p-8 rounded-3xl">
                    <button
                      type="button"
                      className="absolute pb-5 text-gray-400 top-4 right-4 hover:text-gray-500 sm:top-8 sm:right-6 md:top-6 md:right-6 lg:top-8 lg:right-8"
                      onClick={() => setModalStateOpen(false)}
                    >
                      <span className="sr-only">Close</span>
                      <XIcon className="w-6 h-6" aria-hidden="true" />
                    </button>

                    <div className="grid items-start w-full">
                      <SettingsKeyForm setModalState={setModalStateOpen} />
                    </div>
                  </div>
                </div>
              </Transition.Child>
            </div>
          </Dialog>
        </Transition.Root>
      </div>
      <div className="col-xl-12 h-screen">
        <div className="card">
          <div className="card-header">
            <div className="flex justify-between w-screen">
              <div className="">
                <h4 className="card-title">Setting types</h4>
              </div>
              <div className="">
                <button
                  type="button"
                  className="text-white bg-blue-800 hover:bg-blue-900 focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-4 py-2.5 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  onClick={(e) => {
                    e.preventDefault();
                    setModalStateOpen(true);
                  }}
                >
                  <PlusIcon width={20} height={20} />
                </button>
              </div>
            </div>
          </div>
          <div className="card-body">
            {/* Nav tabs */}
            <div className="default-tab">
              <ul className="nav nav-tabs" role="tablist">
                {settingTypes.map((item: KeySetting, index: number) => {
                  return index === 0 ? (
                    <li
                      key={index + " " + item.displayName}
                      className="nav-item"
                    >
                      <a className="nav-link active" data-toggle="tab"
                        href={`#${item.value}`}
                      >
                        {item.displayName}
                      </a>
                    </li>
                  ) : (
                    <li
                      key={index + " " + item.displayName}
                      className="nav-item"
                    >
                      <a
                        onClick={(e) => {
                          e.preventDefault();
                          // addTabContent(item, index);
                        }}
                        className="nav-link"
                        data-toggle="tab"
                        href={`#${item.value}`}
                      >
                        {item.displayName}
                      </a>
                    </li>
                  );
                })}
              </ul>
              <div className="tab-content" id="tab-content">
                {settingTypes.map((item, index) => {
                  return index === 0 ? (
                    <div
                      className="tab-pane fade active show"
                      id={item.value}
                      key={index}
                    >
                      <div className="pt-4">
                        <SettingTypeForm
                          keyCode={item.value}
                          title={item.displayName}
                        />
                      </div>
                    </div>
                  ) : (
                    <div className="tab-pane fade" id={item.value} key={index}>
                      <div className="pt-4">
                        <SettingTypeForm
                          keyCode={item.value}
                          title={item.displayName}
                        />
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SettingTypes;
