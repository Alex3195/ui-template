export interface LookupData {
    id: number | null;
    key: string;
    locale: string;
    value: string;
    fieldName: string;
    objectName: string;
}