import React, { useEffect, useState, Fragment } from "react";
import { APIKit } from "../../../request/requests";
import { SettingTypes } from "../../../types/SettingTypes";
import { useAppSelector } from "../../../app/hooks";
import { selectLanguage } from "../../../features/lang/languageSlice";
import { PlusIcon } from "@heroicons/react/outline";
import { Transition, Dialog } from "@headlessui/react";
import { XIcon } from "@heroicons/react/solid";
import SettingTypeValues from "./SettingTypeValues";
interface SettingsTypeProp {
  title: string;
  keyCode: string;
}
function SettingTypeForm({ title, keyCode }: SettingsTypeProp) {
  const language = useAppSelector(selectLanguage);
  const [settingList, setSettingsList] = useState<SettingTypes[]>([]);
  const [editItem, setEditItem] = useState<SettingTypes>();
  const [settingListResult, setSettingListResult] = useState<SettingTypes[]>(
    []
  );
  const [msg, setMsg] = useState("There is no data");
  const [modalStateOpen, setModalStateOpen] = useState(false);
  const onEdit = (item: SettingTypes) => {
    setEditItem(item);
    setModalStateOpen(true);
  };
  const onDelete = (item: SettingTypes) => {
    APIKit.delete(`/types/${item.id}`)
      .then(({ data }) => {
        // console.log(data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  useEffect(() => {
    APIKit.get(`/types`, {
      params: { key: keyCode, lang: language.language },
    })
      .then(({ data }: any) => {
        if (!data.success) {
          setMsg(data.body);
        } else {
          data.body.sort(function (a: SettingTypes, b: SettingTypes) {
            let x = a.displayName.toLowerCase();
            let y = b.displayName.toLowerCase();
            if (x < y) {
              return -1;
            }
            if (x > y) {
              return 1;
            }
            return 0;
          });
          setSettingsList(data.body);
          setSettingListResult(data.body);
        }
      })
      .catch((error) => {
        console.log(error.response.data.message);
      });
  }, [language.language,modalStateOpen]);
  return (
    <>
      {/* Dialog */}
      <div>
        <Transition.Root show={modalStateOpen} as={Fragment}>
          <Dialog
            as="div"
            className="fixed inset-0 z-10 overflow-y-auto "
            onClose={setModalStateOpen}
          >
            <div
              className="flex min-h-screen text-center border-b md:block md:px-2 lg:px-4 rounded-2xl"
              style={{ fontSize: 0 }}
            >
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Dialog.Overlay className="fixed inset-0 hidden transition-opacity bg-gray-500 bg-opacity-75 md:block" />
              </Transition.Child>

              {/* This element is to trick the browser into centering the modal contents. */}
              <span
                className="hidden md:inline-block md:align-middle md:h-screen"
                aria-hidden="true"
              >
                &#8203;
              </span>
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
                enterTo="opacity-100 translate-y-0 md:scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 translate-y-0 md:scale-100"
                leaveTo="opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
              >
                <div className="flex w-full text-base text-left transition transform md:inline-block md:max-w-2xl md:px-4 md:my-8 md:align-middle lg:max-w-4xl ">
                  <div className="relative flex items-center w-full px-4 pb-8 overflow-hidden bg-white border-b shadow-2xl pt-14 sm:px-6 sm:pt-8 md:p-6 lg:p-8 rounded-3xl">
                    <button
                      type="button"
                      className="absolute pb-5 text-gray-400 top-4 right-4 hover:text-gray-500 sm:top-8 sm:right-6 md:top-6 md:right-6 lg:top-8 lg:right-8"
                      onClick={() => setModalStateOpen(false)}
                    >
                      <span className="sr-only">Close</span>
                      <XIcon className="w-6 h-6" aria-hidden="true" />
                    </button>

                    <div className="grid items-start w-full">
                      <SettingTypeValues
                        setModalState={setModalStateOpen}
                        title={title}
                        keyCode={keyCode}
                        editItem={editItem}
                      />
                    </div>
                  </div>
                </div>
              </Transition.Child>
            </div>
          </Dialog>
        </Transition.Root>
      </div>
      {/* table */}
      <div className="flex flex-col py-2">
        <div className="overflow-x-auto">
          <div className="flex justify-between">
            <div className="px-4 pb-2">
              <h3>{title}</h3>
            </div>
            <div className="px-2 py-2">
              <button
                type="button"
                className="text-white bg-blue-800 hover:bg-blue-900 focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-4 py-2.5 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                onClick={(e) => {
                  e.preventDefault();
                  setEditItem(undefined);
                  setModalStateOpen(true);
                }}
              >
                <PlusIcon width={20} height={20} />
              </button>
            </div>
          </div>
          <div className="inline-block min-w-full py-2 align-middle sm:px-1 lg:px-1">
            <div className="border-b border-gray-200 shadow scrollbar-hide sm:rounded-lg">
              <table className="min-w-full overflow-y-auto divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Display name
                    </th>

                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Code
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Actions
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {settingListResult.length > 0 ? (
                    settingListResult.map(
                      (item: SettingTypes, index: number) => {
                        return (
                          <tr key={index}>
                            <td className="px-6 py-4 whitespace-nowrap">
                              <div className="flex items-center">
                                <div className="ml-4">
                                  <div className="text-sm font-medium text-gray-900">
                                    {item.displayName}
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap">
                              <div className="text-sm text-gray-900">
                                {item.code}
                              </div>
                            </td>
                            <td className="flex justify-start px-6 py-4 text-sm font-medium text-rfirst-letter:ight whitespace-nowrap">
                              <div className="flex justify-start">
                                <button
                                  className="mr-1 shadow btn btn-rounded btn-outline-warning sharp"
                                  onClick={(e) => {
                                    e.preventDefault();
                                    onEdit(item);
                                  }}
                                >
                                  <i className="fa fa-pencil " />
                                </button>
                                <button
                                  className="mr-1 shadow btn btn-rounded btn-outline-danger sharp"
                                  onClick={(e) => {
                                    e.preventDefault();
                                    onDelete(item);
                                  }}
                                >
                                  <i className="fa fa-trash " />
                                </button>
                              </div>
                            </td>
                          </tr>
                        );
                      }
                    )
                  ) : (
                    <tr>
                      <td colSpan={4}>
                        <p className="flex justify-center">{msg}</p>
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SettingTypeForm;
