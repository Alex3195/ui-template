import React, { useState } from "react";
import logo from "../../../assets/images/logo.png";
import logoText from "../../../assets/images/logo-text.png";
import calendarImage from "../../../assets/images/calendar.png";

function SideBar() {
  const [active, setActive] = useState(false);
  return (
    <>
      <div className="nav-header">
        <a href="#/" className="brand-logo">
          <img className="logo-abbr" src={logo} alt="" />
          <img className="logo-compact" src={logoText} alt="" />
          <img className="brand-title" src={logoText} alt="" />
        </a>
        <div className="nav-control">
          <div
            onClick={(e) => {
              e.preventDefault();
              active ? setActive(false) : setActive(true);
            }}
            className="hamburger"
          >
            <span className="line" />
            <span className="line" />
            <span className="line" />
          </div>
        </div>
      </div>
      <div className="deznav">
        <div className="deznav-scroll">
          <ul className="metismenu" id="menu">
            <li>
              <a className="has-arrow ai-icon" href="#/" aria-expanded="false">
                <i className="flaticon-381-networking" />
                <span className="nav-text">Dashboard</span>
              </a>
              <ul aria-expanded="false">
                <li>
                  <a href="/dashboard">Dashboard</a>
                </li>
              </ul>
            </li>
            <li>
              <a className="has-arrow ai-icon" href="#/" aria-expanded="false">
                <i className="flaticon-381-television" />
                <span className="nav-text">Master Data</span>
              </a>
              <ul aria-expanded="false">
                <li>
                  <a href="/users">Users</a>
                </li>
                <li>
                  <a href="/brand">Brand</a>
                </li>
                <li>
                  <a href="/model-category">Model categories</a>
                </li>
                <li>
                  <a href="/model">Model</a>
                </li>
                <li>
                  <a href="/model-attribute">Model attribute</a>
                </li>
              </ul>
            </li>
            <li>
              <a className="has-arrow ai-icon" href="#/" aria-expanded="false">
                <i className="flaticon-381-controls-3" />
                <span className="nav-text">App Data</span>
              </a>
              <ul aria-expanded="false">
                <li>
                  <a href="/topic-category">Topic category</a>
                </li>
                <li>
                  <a href="/topic-theme">Topic themes</a>
                </li>
                <li>
                  <a href="/topic">Topic</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="/settings" className="ai-icon" aria-expanded="false">
                <i className="flaticon-381-settings-2" />
                <span className="nav-text">Settings</span>
              </a>
            </li>
          </ul>
          <div className="add-menu-sidebar">
            <img src={calendarImage} className="mr-3" alt="" />
            <p className="mb-0 font-w500">Create Workout Plan Now</p>
          </div>
        </div>
      </div>
    </>
  );
}

export default SideBar;
