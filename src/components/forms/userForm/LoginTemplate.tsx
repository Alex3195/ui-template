import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { useAppDispatch } from "../../../app/hooks";
import "../../../assets/css/style.css";
import imageLogo from "../../../assets/images/logo-full.png";
import { login } from "../../../features/user/userSlice";
import { APIKit } from "../../../request/requests";
import { User } from "../../../types/user";
import { UserRespone } from "../../../types/userResponseType";
function LoginTemplate() {
  const [userLogin, setUserLogin] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useAppDispatch();
  const naivgate = useNavigate();
  const onSuccess = ({ data }: any) => {
    // Set JSON Web Token on success
    let userResponse = {
      id: data.user.id,
      fullName: data.user.fullName,
      username: data.user.username,
      password: data.user.password,
      isActive: data.user.isActive,
      role: data.user.role,
      token: data.token,
      image: data.image,
    } as UserRespone;
    localStorage.setItem("token", data.token);
    dispatch(login(userResponse));
    naivgate("/dashboard");
  };

  const onFailure = (errors: any) => {
    console.log(errors.response);
  };

  const handleSubmit = () => {
    let user: User;
    user = {
      username: userLogin,
      password: password,
    };
    APIKit.post("/login", JSON.stringify(user), {
      headers: { "Content-Type": "application/json" },
    })
      .then(onSuccess)
      .catch(onFailure);
  };

  return (
    <>
      <div className="authincation h-100">
        <div className="container h-100">
          <div className="pt-40 row justify-content-center h-100 align-items-center">
            <div className="col-md-6">
              <div className="authincation-content">
                <div className="row no-gutters">
                  <div className="col-xl-12">
                    <div className="auth-form">
                      <div className="flex mb-3 text-center">
                        <div className="justify-end">
                          <img src={imageLogo} alt="" />
                        </div>
                      </div>
                      <h4 className="mb-4 text-center text-white">
                        Sign in your account
                      </h4>
                      <form
                        onSubmit={(e) => {
                          e.preventDefault();
                          handleSubmit();
                        }}
                      >
                        <div className="form-group">
                          <label className="mb-1 text-white">
                            <strong>Email</strong>
                          </label>
                          <input
                            type="email"
                            autoComplete="email"
                            className="form-control"
                            value={userLogin}
                            onChange={(e) => {
                              e.preventDefault();
                              console.log(e.target.value);
                              setUserLogin(e.target.value);
                            }}
                            required
                          />
                        </div>
                        <div className="form-group">
                          <label className="mb-1 text-white">
                            <strong>Password</strong>
                          </label>
                          <input
                            type="password"
                            className="form-control"
                            value={password}
                            autoComplete="current-password"
                            onChange={(e) => {
                              e.preventDefault();
                              setPassword(e.target.value);
                            }}
                            required
                          />
                        </div>
                        <div className="mt-4 mb-2 form-row d-flex justify-content-between">
                          <div className="form-group">
                            <div className="ml-1 text-white custom-control custom-checkbox">
                              <input
                                type="checkbox"
                                className="custom-control-input"
                                id="basic_checkbox_1"
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="basic_checkbox_1"
                              >
                                Remember my preference
                              </label>
                            </div>
                          </div>
                          <div className="form-group">
                            <a
                              className="text-white"
                              href="page-forgot-password.html"
                            >
                              Forgot Password?
                            </a>
                          </div>
                        </div>
                        <div className="text-center">
                          <button
                            type="submit"
                            className="bg-white btn text-primary btn-block"
                          >
                            Sign Me In
                          </button>
                        </div>
                      </form>
                      <div className="mt-3 new-account">
                        <p className="text-white">
                          Don't have an account?{" "}
                          <a className="text-white" href="./page-register.html">
                            Sign up
                          </a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/***********************************
  Scripts
    ************************************/}
      {/* Required vendors */}
    </>
  );
}

export default LoginTemplate;
