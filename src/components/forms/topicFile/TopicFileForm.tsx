import { XIcon } from "@heroicons/react/solid";
import { useNavigate } from "react-router";
import { APIKit } from "../../../request/requests";
async function uploadAsync(file: File, topicId: any) {
  let formData = new FormData();
  formData.append("file", file);
  formData.append("topicId", topicId);
  formData.append("type", file.type);

  let res = await APIKit.post("/topic/file", formData);
  return res.data.body;
}
function TopicFileForm({
  displayIconId,
  setDisplayIconId,
  remove,
  itemId,
}: any) {
  const navigate = useNavigate();
  const upload = (e: any, id: any) => {
    if (e.target.files[0].size <= 300000000) {
      let file = e.target.files[0];
      let render = new FileReader();
      let filename = e.target.files[0].name;
      let type: string = e.target.files[0].type;
      console.log(type);

      render.onload = function (e) {
        uploadAsync(file, id).then((res) => {
          let displayLogoImg =
            "https://media.istockphoto.com/vectors/file-folder-flat-vector-icon-vector-id881436122?k=20&m=881436122&s=170667a&w=0&h=o4v7w7p2_3CGC-au3B2tjew22gyssDFH42CCItxecK0=";
          if (type.includes("image")) {
            displayLogoImg =
              "https://icon-library.com/images/icon-gallery/icon-gallery-5.jpg";
          }
          if (type.includes("video")) {
            displayLogoImg =
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTDDDp3nQATy8fBcj0dO9fhf3Coynv1wr9xbQ&usqp=CAU";
          }
          if (type.includes("audio")) {
            displayLogoImg =
              "https://www.shareicon.net/data/2017/01/23/874891_speaker_512x512.png";
          }
          if (
            type.includes("application/pdf") ||
            type.includes("sheet") ||
            type.includes("document")
          ) {
            displayLogoImg =
              "https://freepikpsd.com/file/2019/10/doc-to-png-3-Transparent-Images.png";
          }
          let obj = {
            displayIconId: res,
            displayLogoImage: displayLogoImg,
            filename: filename,
          };
          setDisplayIconId([...displayIconId, obj]);
        });
      };
      render.readAsDataURL(file);
    } else {
      e.target.value = "";
      alert("please uploD LESS THAN 5MB");
    }
  };

  return (
    <div className="flex p-1">
      <div className="px-2">
        <label className="block px-2 text-sm font-medium text-gray-700">
          Topic File
        </label>
        <div className="flex justify-center px-6 pt-5 pb-6 mt-1 border-2 border-gray-300 border-dashed rounded-md">
          <div className="space-y-1 text-center">
            <svg
              className="w-12 h-12 mx-auto text-gray-400"
              stroke="currentColor"
              fill="none"
              viewBox="0 0 48 48"
              aria-hidden="true"
            >
              <path
                d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <div className="flex text-sm text-gray-600">
              <label
                htmlFor={`file-upload`}
                className="relative font-medium text-indigo-600 bg-white rounded-md cursor-pointer hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
              >
                <span>Upload a file</span>
                <input
                  id={`file-upload`}
                  name="originalLogo"
                  type="file"
                  className="sr-only"
                  onChange={(e) => {
                    e.preventDefault();
                    if (displayIconId.length > 2) {
                      alert("You can not upload more than 3 files");
                    } else {
                      upload(e, itemId);
                    }
                  }}
                />
              </label>
              <p className="pl-1">or drag and drop</p>
            </div>
            <p className="text-xs text-gray-500">all type of file up to 5MB</p>
          </div>
        </div>
      </div>
      <div className="p-4 ">
        <div>
          {displayIconId ? (
            displayIconId.map((item: any, index: number) => {
              return (
                <div key={index} className="py-2">
                  <div className="px-2 flex">
                    <img
                      src={item.displayLogoImage}
                      alt=""
                      width={50}
                      height={50}
                    />
                    <span
                      className="cursor-pointer border-2 rounded-full h-min p-1 text-red-300 border-red-300 "
                      onClick={(e) => {
                        e.preventDefault();
                        remove(item);
                      }}
                    >
                      <XIcon width={10} height={10} />
                    </span>
                    <p className="text-sm px-4 my-3 w-full">{item.filename}</p>
                  </div>
                </div>
              );
            })
          ) : (
            <></>
          )}
        </div>
      </div>
    </div>
  );
}

export default TopicFileForm;
