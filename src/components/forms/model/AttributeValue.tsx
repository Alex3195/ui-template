import React, { useEffect, useState } from "react";
import { useAppSelector } from "../../../app/hooks";
import { selectLanguage } from "../../../features/lang/languageSlice";
import { APIKit } from "../../../request/requests";
import MultiSelectComponent from "../../MultiSelect/MultiSelectComponent";
async function searchOption(searchableData: string) {
  return await APIKit.get("/model/attribute/option/autocomplete", {
    params: { keyword: searchableData },
  });
}
function AttributeValue({ item, getOptions }: any) {
  const [result, setResult] = useState<any>();
  const language = useAppSelector(selectLanguage);
  const [value, setValue] = useState<any>();
  const [selectItem, setSelectItem] = useState<any[]>([]);
  const [filteredSuggestionsData, setFilteredSuggestionsData] = useState<any>(
    []
  );
  const getOptionData = (text: string, isEnter: boolean) => {
    let data = searchOption(text);
    data.then(({ data }: any) => {
      if (data.body.length > 0) {
        setFilteredSuggestionsData(data.body);
      } else {
        if (isEnter) {
          let obj = {
            key: "",
            value: text,
          };
          setSelectItem([...selectItem, obj]);
        }
      }
    });
  };

  useEffect(() => {
    switch (item.attributeType) {
      case "select":
        if (item.options) {
          setSelectItem(item.options);
          setFilteredSuggestionsData(item.options);
        }
        setResult(
          <MultiSelectComponent
            items={filteredSuggestionsData}
            setItems={setFilteredSuggestionsData}
            optionData={getOptionData}
            selectedItems={selectItem}
            setSelected={setSelectItem}
            setSelectedOptions={getOptions}
          />
        );

        break;
      case "multi_select":
        if (item.options) {
          setSelectItem(item.options);
          setFilteredSuggestionsData(item.options);
        }
        setResult(
          <MultiSelectComponent
            items={filteredSuggestionsData}
            setItems={setFilteredSuggestionsData}
            optionData={getOptionData}
            selectedItems={selectItem}
            setSelected={setSelectItem}
            setSelectedOptions={getOptions}
          />
        );
        break;
      case "date":
        let date = new Date().toISOString().split("T")[0];
        // setValue(date);
        let dateRes = (
          <input
            type="date"
            value={value}
            onChange={(e) => {
              setValue(e.target.value);
            }}
            className="form-control"
          />
        );
        setResult(dateRes);
        break;
      case "date_time":
        let dateTime = (
          <input
            value={value}
            type="datetime-local"
            onChange={(e) => {
              setValue(e.target.value);
            }}
            className="form-control"
          />
        );
        setResult(dateTime);
        break;
      case "number":
        // setValue(0);
        let number = (
          <input
            type="number"
            value={value}
            onChange={(e) => {
              setValue(parseInt(e.target.value));
            }}
            className="form-control"
          />
        );
        setResult(number);
        break;
      case "text_are":
        let textAre = (
          <textarea
            value={value}
            className="w-full border-slate-300 rounded-xl"
            name="content"
            id="content"
            rows={5}
            onChange={(e) => {
              setValue(e.target.value);
            }}
          />
        );
        setResult(textAre);
        break;

      default:
        let res = <input type="text" className="form-control" />;
        setResult(res);
    }
  }, [filteredSuggestionsData, selectItem, value]);
  return <>{result}</>;
}

export default AttributeValue;
