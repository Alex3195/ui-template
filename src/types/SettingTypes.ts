export interface SettingTypes {
  id: number|null;
  displayName: string;
  key: string;
  code: string;
}
