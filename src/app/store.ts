import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import thunk from 'redux-thunk';
import { combineReducers } from 'redux';
import persistStore from 'redux-persist/lib/persistStore';
import userReducer from '../features/user/userSlice';
import languageReducer from '../features/lang/languageSlice';
import attributeReducer from '../features/attribute/attributeSlice';
const reducer = combineReducers(
  {
    user: userReducer,
    language: languageReducer,
    attribute: attributeReducer
  }
)
const persistConfig = {
  key: 'user',
  storage
}

const persistedReducer = persistReducer(persistConfig, reducer)

export const store = configureStore({
  reducer: persistedReducer,
  middleware: [thunk]
});
export const persistor = persistStore(store)
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
