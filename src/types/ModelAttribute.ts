import { Map } from "typescript";

export interface ModelAttribute {
    id: number;
    defaultDisplayName: string;
    displayName: string;
    attributeType: string;
    unit: string;
    options: any;
}