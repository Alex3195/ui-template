export interface UserRespone {
  id: number | null;
  role: any;
  token: string | null;
  fullName: string;
  username: string;
  password: string;
  isActive: boolean | null;
  image: File | any;
  imageId: number | null;
}
