import React, { Fragment, useState } from 'react'

import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { logout } from '../../features/user/userSlice'
import { useNavigate } from 'react-router'
import { Disclosure, Menu, Transition } from '@headlessui/react'
import { BellIcon, MenuIcon, XIcon } from '@heroicons/react/outline'
import { selectLanguage, setLanguage } from '../../features/lang/languageSlice';
import { CogIcon, PhotographIcon } from '@heroicons/react/solid'
const user = {
    name: 'Tom Cook',
    email: 'tom@example.com',
    imageUrl:
        'https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
}
const langNavigation = [
    { icon: 'https://api.guest.az/upload/photo/2019/04/29/20190429100358-58707940.png', local: 'en', text: 'English' },
    { icon: 'https://icon-library.com/images/russian-2-512_111210.png', local: 'ru', text: 'Russian' },
    { icon: 'https://vectorflags.s3.amazonaws.com/flags/uz-circle-01.png', local: 'uz', text: 'Uzbek' },

]
const setiingsList = [
    {
        text: 'Image types',
        url: '/image-types',
        icon: <PhotographIcon className="w-6 h-6" aria-hidden="true" />
    },
    {
        text: 'Image setting',
        url: '/image-settings',
        icon: <CogIcon className="w-6 h-6" aria-hidden="true" />
    },
    {
        text: 'setting2',
        url: '#/image-types',
        icon: <CogIcon className="w-6 h-6" aria-hidden="true" />
    },
]

const userNavigation = [
    { name: 'Your Profile', href: '#' },
    { name: 'Settings', href: '#' },
    { name: 'Sign out', href: '#' },
]

function classNames(...classes: any) {
    return classes.filter(Boolean).join(' ')
}

function Navbar({ navigationItems, setNavigationItems }: any) {
    const dispatch = useAppDispatch()
    const language = useAppSelector(selectLanguage);
    const navigate = useNavigate();
    const [flagIcon, setFlagIcon] = useState<any>(language.language.icon)
    const handleLangNavigation = (e: React.MouseEvent, item: any) => {
        setFlagIcon(item.icon);
        dispatch(setLanguage(item));
    }
    const handleNavigation = (e: React.MouseEvent, item: any) => {
        navigationItems.forEach((navItem: any) => {
            if (navItem === item) {
                navItem.current = true
            } else {
                navItem.current = false
            }
        })
        setNavigationItems(navigationItems)
    }
    const handleUserNaivgationAction = (e: any, name: string) => {
        e.preventDefault();

        if (name.match('Sign out')) {
            dispatch(logout())
            navigate('/')
        }

    }

    return (
        <>
            <Disclosure as="nav" className="sticky inset-x-0 top-0 h-16 bg-gray-800">
                {({ open }) => (
                    <>
                        <div className="max-w-full px-4 mx-auto sm:px-6 lg:px-8">
                            <div className="flex items-center justify-between h-16">
                                <div className="flex items-center">
                                    <div className="flex-shrink-0">
                                        <img
                                            className="w-8 h-8"
                                            src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
                                            alt="Workflow"
                                        />
                                    </div>
                                    <div className="hidden md:block">
                                        <div className="flex items-baseline ml-10 space-x-4">
                                            {navigationItems.map((item: any) => (
                                                <a
                                                    key={item.name}
                                                    href={item.href}
                                                    onClick={(e) => { handleNavigation(e, item) }}
                                                    className={classNames(
                                                        item.current
                                                            ? 'bg-gray-900 text-white'
                                                            : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                                                        'px-3 py-2 rounded-md text-sm font-medium'
                                                    )}
                                                    aria-current={item.current ? 'page' : undefined}
                                                >
                                                    {item.name}
                                                </a>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                <div className="hidden md:block">
                                    <div className="flex items-center ml-4 md:ml-6">
                                        <button
                                            type="button"
                                            className="p-1 text-gray-400 bg-gray-800 rounded-full hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                                        >
                                            <span className="sr-only">View notifications</span>
                                            <BellIcon className="w-6 h-6" aria-hidden="true" />
                                        </button>

                                        {/* Settings dropdown */}
                                        <Menu as="div" className="relative ml-3">
                                            <div>
                                                <Menu.Button className="p-1 text-gray-400 bg-gray-800 rounded-full hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                                                    <span className="sr-only">Open language menu</span>
                                                    <CogIcon className="w-6 h-6" aria-hidden="true" />
                                                </Menu.Button>
                                            </div>
                                            <Transition
                                                as={Fragment}
                                                enter="transition ease-out duration-100"
                                                enterFrom="transform opacity-0 scale-95"
                                                enterTo="transform opacity-100 scale-100"
                                                leave="transition ease-in duration-75"
                                                leaveFrom="transform opacity-100 scale-100"
                                                leaveTo="transform opacity-0 scale-95"
                                            >
                                                <Menu.Items className="absolute right-0 w-48 py-1 mt-2 origin-top-right bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                                                    {setiingsList.map((item, index) => (
                                                        <Menu.Item key={index}>
                                                            {({ active }) => (
                                                                <div className={
                                                                    classNames(
                                                                        '',
                                                                        'flex justify-between w-full cursor-pointer block px-4 py-2 text-sm text-gray-700')
                                                                }

                                                                    onClick={(e) => {
                                                                        console.log(item);
                                                                    }}
                                                                >
                                                                    <a
                                                                        href={item.url}
                                                                        className={classNames(
                                                                            '',
                                                                            'block px-4 py-2 text-sm text-gray-700'
                                                                        )}
                                                                    >
                                                                        {item.text}
                                                                    </a>
                                                                    <div className="justify-end w-8 h-8 mr-2 rounded-h-full" >
                                                                        {item.icon}
                                                                    </div>
                                                                </div>

                                                            )}
                                                        </Menu.Item>
                                                    ))}
                                                </Menu.Items>
                                            </Transition>
                                        </Menu>

                                        {/* Language dropdown */}
                                        <Menu as="div" className="relative ml-3">
                                            <div>
                                                <Menu.Button className="flex items-center max-w-xs text-sm bg-gray-800 rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                                                    <span className="sr-only">Open language menu</span>
                                                    <img className="w-8 h-8 rounded-full" src={flagIcon} alt="" />
                                                </Menu.Button>
                                            </div>
                                            <Transition
                                                as={Fragment}
                                                enter="transition ease-out duration-100"
                                                enterFrom="transform opacity-0 scale-95"
                                                enterTo="transform opacity-100 scale-100"
                                                leave="transition ease-in duration-75"
                                                leaveFrom="transform opacity-100 scale-100"
                                                leaveTo="transform opacity-0 scale-95"
                                            >
                                                <Menu.Items className="absolute right-0 w-48 py-1 mt-2 origin-top-right bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                                                    {langNavigation.map((item) => (
                                                        <Menu.Item key={item.local}>
                                                            {({ active }) => (
                                                                <div className={
                                                                    classNames(
                                                                        item === language.language ? 'bg-gray-200 ' : '',
                                                                        'flex justify-between w-full cursor-pointer block px-4 py-2 text-sm text-gray-700')
                                                                }

                                                                    onClick={(e) => handleLangNavigation(e, item)}
                                                                >
                                                                    <a
                                                                        href='#/'
                                                                        className={classNames(
                                                                            item === language.language ? 'bg-gray-200' : '',
                                                                            'block px-4 py-2 text-sm text-gray-700'
                                                                        )}
                                                                    >
                                                                        {item.text}
                                                                    </a>
                                                                    <img className="justify-end w-8 h-8 mr-2 rounded-h-full" src={item.icon} alt="" />
                                                                </div>

                                                            )}
                                                        </Menu.Item>
                                                    ))}
                                                </Menu.Items>
                                            </Transition>
                                        </Menu>

                                        {/* Profile dropdown */}
                                        <Menu as="div" className="relative ml-3">
                                            <div>
                                                <Menu.Button className="flex items-center max-w-xs text-sm bg-gray-800 rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                                                    <span className="sr-only">Open user menu</span>
                                                    <img className="w-8 h-8 rounded-full" src={user.imageUrl} alt="" />
                                                </Menu.Button>
                                            </div>
                                            <Transition
                                                as={Fragment}
                                                enter="transition ease-out duration-100"
                                                enterFrom="transform opacity-0 scale-95"
                                                enterTo="transform opacity-100 scale-100"
                                                leave="transition ease-in duration-75"
                                                leaveFrom="transform opacity-100 scale-100"
                                                leaveTo="transform opacity-0 scale-95"
                                            >
                                                <Menu.Items className="absolute right-0 w-48 py-1 mt-2 origin-top-right bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                                                    {userNavigation.map((item) => (
                                                        <Menu.Item key={item.name}>
                                                            {({ active }) => (
                                                                <a
                                                                    href={item.href}
                                                                    onClick={(e) => handleUserNaivgationAction(e, item.name)}
                                                                    className={classNames(
                                                                        active ? 'bg-gray-100' : '',
                                                                        'block px-4 py-2 text-sm text-gray-700'
                                                                    )}
                                                                >
                                                                    {item.name}
                                                                </a>
                                                            )}
                                                        </Menu.Item>
                                                    ))}
                                                </Menu.Items>
                                            </Transition>
                                        </Menu>
                                    </div>
                                </div>
                                <div className="flex -mr-2 md:hidden">
                                    {/* Mobile menu button */}
                                    <Disclosure.Button className="inline-flex items-center justify-center p-2 text-gray-400 bg-gray-800 rounded-md hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                                        <span className="sr-only">Open main menu</span>
                                        {open ? (
                                            <XIcon className="block w-6 h-6" aria-hidden="true" />
                                        ) : (
                                            <MenuIcon className="block w-6 h-6" aria-hidden="true" />
                                        )}
                                    </Disclosure.Button>
                                </div>
                            </div>
                        </div>

                        <Disclosure.Panel className="md:hidden">
                            <div className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
                                {navigationItems.map((item: any) => (
                                    <Disclosure.Button
                                        key={item.name}
                                        as="a"
                                        href={item.href}
                                        onClick={(e: React.MouseEvent) => { handleNavigation(e, item) }}
                                        className={classNames(
                                            item.current ? 'bg-gray-900 text-white' : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                                            'block px-3 py-2 rounded-md text-base font-medium'
                                        )}
                                        aria-current={item.current ? 'page' : undefined}
                                    >
                                        {item.name}
                                    </Disclosure.Button>
                                ))}
                            </div>
                            <div className="pt-4 pb-3 border-t border-gray-700">
                                <div className="flex items-center px-5">
                                    <div className="flex-shrink-0">
                                        <img className="w-10 h-10 rounded-full" src={user.imageUrl} alt="" />
                                    </div>
                                    <div className="ml-3">
                                        <div className="text-base font-medium leading-none text-white">{user.name}</div>
                                        <div className="text-sm font-medium leading-none text-gray-400">{user.email}</div>
                                    </div>
                                    <button
                                        type="button"
                                        className="flex-shrink-0 p-1 ml-auto text-gray-400 bg-gray-800 rounded-full hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                                    >
                                        <span className="sr-only">View notifications</span>
                                        <BellIcon className="w-6 h-6" aria-hidden="true" />
                                    </button>
                                </div>

                                <div className="px-2 mt-3 space-y-1">
                                    {userNavigation.map((item) => (
                                        <Disclosure.Button
                                            key={item.name}
                                            as="a"
                                            href={item.href}
                                            className="block px-3 py-2 text-base font-medium text-gray-400 rounded-md hover:text-white hover:bg-gray-700"
                                            onClick={() => {
                                                console.log('----', item.name);

                                            }}
                                        >
                                            {item.name}
                                        </Disclosure.Button>
                                    ))}
                                </div>
                            </div>
                        </Disclosure.Panel>
                    </>
                )}
            </Disclosure>
        </>
    )
}

export default Navbar
