import { Backspace } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router";
import { useAppSelector } from "../../../app/hooks";
import { selectLanguage } from "../../../features/lang/languageSlice";
import { APIKit } from "../../../request/requests";
import { Topic } from "../../../types/Topic";
import { TopicTheme } from "../../../types/TopicTheme";
import TopicFileForm from "../topicFile/TopicFileForm";

function saveData(
  data: any,
  onSuccess: any,
  onFailure: any,
  translationLanguge: any
) {
  if (translationLanguge) {
    APIKit.post("/topic/translation/" + translationLanguge, data)
      .then(onSuccess)
      .catch(onFailure);
  } else {
    APIKit.post("/topic", data).then(onSuccess).catch(onFailure);
  }
}
function saveTopicFiles(obj: any) {
  APIKit.post("//topic/file", obj).then(({ data }: any) => {
    console.log("====================================");
    console.log(data.body);
    console.log("====================================");
  });
}
function TopicDetails() {
  const language = useAppSelector(selectLanguage);
  const location = useLocation();
  const navigate = useNavigate();
  const [formTitle, setFormTitle] = useState("");
  const [title, setTitle] = useState("");
  const [subTitle, setSubTitle] = useState("");
  const [disableSaveButton, setDisableSaveButton] = useState(true);
  const [defaultTitle, setDefaultTitle] = useState("");
  const [itemData, setItemData] = useState<Topic>();
  const [translationLanguge, setTranslationLanguge] = useState<any>();
  const [topicThemes, setTopicThemes] = useState<TopicTheme[]>([]);
  const [topicThemeId, setTopicThemeId] = useState<any>();
  const [displayIconId, setDisplayIconId] = useState<any[]>([]);

  useEffect(() => {
    if (location.state) {
      setTranslationLanguge(language.language);
      setDisableSaveButton(false);
      let obj = location.state as Topic;
      console.log(obj);

      setItemData(obj);
      setDefaultTitle(obj.defaultTitle);
      setFormTitle(obj.defaultTitle);
      setTitle(obj.title ? obj.title : "");
      setSubTitle(obj.subTitle ? obj.subTitle : "");
      setTopicThemeId(obj.topicThemeId ? obj.topicThemeId : 0);
    }
    APIKit.get("/topic/themes")
      .then(({ data }: any) => {
        setTopicThemes(data.body);
      })
      .catch((error) => {
        console.log(error.response);
      });
  }, []);
  const remove = (item: any) => {
    const filtered = displayIconId.filter((e) => e !== item);
    setDisplayIconId(filtered);
  };
  const onSuccess = ({ data }: any) => {
    console.log(data.body);
    setItemData(data.body);
    navigate("/topic");
  };
  const onFailure = (error: any) => {
    console.log(error.response);
  };
  const getTopicCategoryByIdAndTranslationLanguage = async (
    id: any,
    lang: any
  ) => {
    APIKit.get(`/topic/${id}/${lang}`)
      .then(({ data }): any => {
        setDisableSaveButton(false);
        setItemData(data.body);
        setFormTitle(data.body.defaultTitle);
        setDisableSaveButton(data.body.defaultTitle ? false : true);
        setDefaultTitle(data.body.defaultTitle ? data.body.defaultTitle : "");
        setTitle(data.body.title ? data.body.title : "");
        setSubTitle(data.body.subTitle ? data.body.subTitle : "");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const handleSave = () => {
    const obj = {
      id: itemData ? itemData.id : null,
      defaultTitle: defaultTitle,
      title: title,
      subTitle: subTitle,
      topicThemeId: topicThemeId,
    };
    if (itemData) {
      let fileIds = displayIconId.map((item: any) => {
        return item.displayIconId;
      });
      let topicFileObj = {
        topicId: itemData.id,
        fileIds: fileIds,
      };
      saveTopicFiles(topicFileObj);
    }
    saveData(obj, onSuccess, onFailure, translationLanguge);
  };
  return (
    <>
      <div className="px-5 content-body">
        <div className="card">
          <div className="card-header">
            <div className="flex justify-start ">
              <div className="mt-2 ml-4">
                <h4 className="card-title">
                  {formTitle ? formTitle : "Title"}
                </h4>
                <p className="py-1 text-sm">{subTitle}</p>
              </div>
            </div>
            <div className="flex">
              <div
                className="flex px-3 py-1 text-indigo-800 border-2 border-indigo-800 cursor-pointer rounded-3xl hover:bg-indigo-100"
                onClick={(e) => {
                  e.preventDefault();
                  navigate("/topic");
                }}
              >
                <Backspace width={20} />
                <button className="pl-2"> Back</button>
              </div>
              <div
                className={
                  disableSaveButton
                    ? "px-3 py-1 mx-2 text-indigo-800 border-2 border-indigo-800 rounded-3xl bg-indigo-100"
                    : "px-3 py-1 mx-2 text-indigo-800 border-2 border-indigo-800 rounded-3xl hover:bg-indigo-100"
                }
              >
                <button disabled={disableSaveButton} onClick={handleSave}>
                  Save
                </button>
              </div>
            </div>
          </div>

          <div className="card-body">
            {/* Nav tabs */}
            <div className="default-tab">
              <ul className="nav nav-tabs" role="tablist">
                <li className="nav-item">
                  <a
                    className="nav-link active"
                    data-toggle="tab"
                    href="#default"
                  >
                    Default
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#basic-info">
                    Basic info
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" data-toggle="tab" href="#topic-file">
                    File
                  </a>
                </li>
              </ul>
              <div className="tab-content">
                <div
                  className="tab-pane fade active show"
                  id="default"
                  role="tabpanel"
                >
                  <div className="row">
                    <div className="pt-10 col-sm-10">
                      <div className="flex py-2">
                        <label
                          htmlFor="defualtName"
                          className="w-40 text-slate-600"
                        >
                          Default title{" "}
                        </label>
                        <input
                          type="text"
                          className="w-full mr-10 rounded-2xl border-slate-300"
                          value={defaultTitle}
                          onChange={(e) => {
                            e.preventDefault();
                            setFormTitle(e.target.value);
                            setDefaultTitle(e.target.value);

                            if (e.target.value === "") {
                              setDisableSaveButton(true);
                            } else {
                              setDisableSaveButton(false);
                            }
                          }}
                        />
                      </div>
                      {topicThemes ? (
                        <div className="flex py-2">
                          <label
                            htmlFor="modelategory"
                            className="w-40 text-slate-600"
                          >
                            Theme{" "}
                          </label>
                          <select
                            id="modelategory"
                            value={topicThemeId}
                            onChange={(e) => {
                              e.preventDefault();
                              setTopicThemeId(parseInt(e.target.value));
                            }}
                            className="w-full mr-10 rounded-2xl border-slate-300"
                          >
                            <option value={0}>Select theme</option>
                            {topicThemes.map(
                              (themeItem: TopicTheme, index: number) => {
                                return (
                                  <option key={index} value={themeItem.id}>
                                    {themeItem.defaultTitle}
                                  </option>
                                );
                              }
                            )}
                          </select>
                        </div>
                      ) : (
                        <></>
                      )}
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade " id="basic-info" role="tabpanel">
                  <div className="row">
                    <div className="col-sm-9">
                      <div className="pt-10">
                        <div className="flex py-2">
                          <label
                            htmlFor="defualtName"
                            className="w-40 text-slate-600"
                          >
                            Title{" "}
                          </label>
                          <input
                            type="text"
                            className="w-full mr-10 rounded-2xl border-slate-300"
                            value={title}
                            onChange={(e) => {
                              setTitle(e.target.value);
                            }}
                          />
                        </div>
                        <div className="flex py-2">
                          <label
                            htmlFor="defualtName"
                            className="w-40 text-slate-600"
                          >
                            SubTilte{" "}
                          </label>
                          <input
                            type="text"
                            className="w-full mr-10 rounded-2xl border-slate-300"
                            value={subTitle}
                            onChange={(e) => {
                              setSubTitle(e.target.value);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-3">
                      <fieldset className="form-group">
                        <div className="row pt-10">
                          <div className="col-sm-9">
                            <label className="col-form-label  pt-0">
                              Language
                            </label>
                            <div className="form-check">
                              {language.language === "uz" ? (
                                <input
                                  className="form-check-input cursor-pointer"
                                  id="lang-radio"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="uz"
                                  defaultChecked={true}
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicCategoryByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              ) : (
                                <input
                                  className="form-check-input cursor-pointer"
                                  id="lang-radio"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="uz"
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicCategoryByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              )}
                              <label className="form-check-label">Uzbek</label>
                            </div>
                            <div className="form-check cursor-pointer">
                              {language.language === "ru" ? (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="ru"
                                  defaultChecked={true}
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicCategoryByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              ) : (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="ru"
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicCategoryByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              )}
                              <label className="form-check-label">
                                Russian
                              </label>
                            </div>
                            <div className="form-check disabled">
                              {language.language === "en" ? (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="en"
                                  defaultChecked={true}
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicCategoryByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              ) : (
                                <input
                                  id="lang-radio"
                                  className="form-check-input cursor-pointer"
                                  type="radio"
                                  name="basicInfoLangRadios"
                                  defaultValue="en"
                                  onChange={(e) => {
                                    e.preventDefault();
                                    setTranslationLanguge(e.target.value);
                                    getTopicCategoryByIdAndTranslationLanguage(
                                      itemData?.id,
                                      e.target.value
                                    );
                                  }}
                                />
                              )}
                              <label className="form-check-label">
                                English
                              </label>
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </div>
                  </div>
                </div>
                <div className="tab-pane fade" id="topic-file">
                  <div className="pt-4">
                    {itemData ? (
                      <TopicFileForm
                        displayIconId={displayIconId}
                        setDisplayIconId={setDisplayIconId}
                        remove={remove}
                        itemId={itemData.id}
                      />
                    ) : (
                      <></>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default TopicDetails;
