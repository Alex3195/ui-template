import React, { useEffect, useState } from "react";
import { NavigateFunction, useLocation, useNavigate } from "react-router";
import { ImageSettingDto } from "../../../features/user/imageSettingDto";
import { APIKit } from "../../../request/requests";
import staticImage from "../../../assets/images/images.png";
import { useAppSelector } from "../../../app/hooks";
import { selectLanguage } from "../../../features/lang/languageSlice";
import { Backspace } from "@material-ui/icons";
import { ModelCategory } from "../../../types/ModelCategory";
import ReactPlayer from "react-player";
async function uploadAsync(file: File, type: string) {
  let formData = new FormData();
  formData.append("image", file);

  let res = await APIKit.post("/image/file", formData);
  return res.data.body;
}
async function saveModelCategoryData(
  modelCategory: ModelCategory,
  language: any,
  navigate: NavigateFunction
) {
  language
    ? APIKit.post("/model/category/translation/" + language, modelCategory)
        .then(({ data }) => {
          console.log(data);
          navigate("/model-category");
        })
        .catch(({ respone }) => {
          return respone;
        })
    : modelCategory.id
    ? APIKit.put("/model/category", modelCategory)
        .then(({ data }) => {
          console.log(data);
          navigate("/model-category");
        })
        .catch(({ respone }) => {
          return respone;
        })
    : APIKit.post("/model/category", modelCategory)
        .then(({ data }) => {
          console.log(data);
          navigate("/model-category");
        })
        .catch(({ respone }) => {
          return respone;
        });
}

function ModelCategoryDetails() {
  const location = useLocation();
  const navigate = useNavigate();
  const language = useAppSelector(selectLanguage);
  const [disableSaveButton, setDisableSaveButton] = useState(true);
  const [modelCategoryItem, setModelCategoryItem] = useState<ModelCategory>();
  const [formTitle, setFormTitle] = useState("Title");
  const [defaultName, setDefaultName] = useState("");
  const [name, setName] = useState<any>("");
  const [description, setDescription] = useState<any>("");
  const [content, setContent] = useState<any>("");
  const [displayLogoImage, setDisplayLogoImage] = useState<any>();
  const [displayHeaderFile, setDisplayHeaderFile] = useState<any>();
  const [translationLanguage, setTranslationLanguge] = useState("");
  const [imageSettings, setImageSettings] = useState<ImageSettingDto[]>([]);
  const [displayHeaderId, setDisplayHeaderId] = useState<any>(null);
  const [displayIconId, setDisplayIconId] = useState<any>(null);
  const [loading, setLoading] = useState(true);
  const [parentList, setParentList] = useState<ModelCategory[]>([]);
  const [parentId, setParentId] = useState<number>(0);
  useEffect(() => {
    if (location.state) {
      let data = location.state as ModelCategory;
      setTranslationLanguge(language.language);
      setModelCategoryItem(data);
      setFormTitle(data.defaultName);
      setDisableSaveButton(data.defaultName ? false : true);
      setDefaultName(data.defaultName);
      setName(data.name ? data.name : "");
      setDescription(data.description ? data.description : "");
      setContent(data.content ? data.content : "");
      setDisplayIconId(data?.displayIconId);
      setDisplayHeaderId(data.displayHeaderId);
      if (data.parentId) setParentId(data.parentId);

      APIKit.get(`/model/category/${language.language}`, {
        params: { id: data.id },
      })
        .then(({ data }: any) => {
          let array = data.body.filter(function (el: ModelCategory) {
            if (el.id !== modelCategoryItem?.id) {
              return el;
            }
          });
          setParentList(array);
        })
        .catch((error) => {
          console.log("====================================");
          console.log(error);
          console.log("====================================");
        });
    } else {
      APIKit.get("/model/category/")
        .then(({ data }: any) => {
          let array = data.body.filter(function (el: ModelCategory) {
            if (el.id !== modelCategoryItem?.id) {
              return el;
            }
          });
          setParentList(array);
        })
        .catch((error) => {
          console.log("====================================");
          console.log(error);
          console.log("====================================");
        });
    }

    APIKit.get("/image/setting/i_model_category")
      .then(({ data }: any) => {
        setImageSettings(data.body);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {
      console.log("destroyed");
    };
  }, [language]);
  const getModelCategoryByIdAndTranslationLanguage = async (
    id: any,
    lang: any
  ) => {
    APIKit.get(`/model/category/${id}/${lang}`)
      .then(({ data }): any => {
        setModelCategoryItem(data.body);
        setFormTitle(data.body.defaultName);
        setDisableSaveButton(data.body.defaultName ? false : true);
        setDefaultName(data.body.defaultName);
        setName(data.body.name ? data.body.name : "");
        setDescription(data.body.description ? data.body.description : "");
        setContent(data.body.content ? data.body.content : "");
        setDisplayIconId(data.body?.displayIconId);
        setDisplayHeaderId(data.body?.displayHeaderId);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const validateUpload = (e: any, Width: number, Height: number) => {
    let fileTypeIsCorrect =
      e.target.files[0].type === "image/png" ||
      e.target.files[0].type === "image/gif" ||
      e.target.files[0].type === "image/jpeg" ||
      e.target.files[0].type === "video/mp4" ||
      e.target.files[0].type === "video/mvk" ||
      e.target.files[0].type === "audio/mp3";

    if (
      e.target.files[0].type === "image/png" ||
      e.target.files[0].type === "image/gif" ||
      e.target.files[0].type === "image/jpeg"
    ) {
      var files = e.target.files;
      var height = 0;
      var width = 0;
      var _URL = window.URL || window.webkitURL;
      for (var i = 0; i < files.length; i++) {
        var img = new Image();
        img.onload = function () {
          height = img.height;
          width = img.width;
        };
        img.src = _URL.createObjectURL(files[i]);
      }
    }
    return fileTypeIsCorrect;
  };
  const upload = (e: any, displayname: string, fileTypeCode: string) => {
    let id: any;
    if (e.target.files[0].size <= 50000000) {
      let file = e.target.files[0];
      let render = new FileReader();
      render.onload = function (e) {
        if (displayname === "Display icon") {
          uploadAsync(file, fileTypeCode).then((res) => {
            setDisplayIconId(res);
          });
          setDisplayLogoImage(e.target?.result);
        }
        if (displayname === "Display header") {
          uploadAsync(file, fileTypeCode).then((res) => {
            setDisplayHeaderId(res);
          });
          setDisplayHeaderId(id);
          setDisplayHeaderFile(e.target?.result);
        }
      };
      render.readAsDataURL(file);
    } else {
      e.target.value = "";
      alert("please uploD LESS THAN 5MB");
    }
  };
  const handleSave = (e: React.MouseEvent) => {
    e.preventDefault();
    let modelCategoryData = {
      id: modelCategoryItem?.id,
      defaultName: defaultName,
      name: name,
      description: description,
      content: content,
      displayIconId: displayIconId,
      displayHeaderId: displayHeaderId,
      parentId: parentId,
    } as ModelCategory;

    saveModelCategoryData(modelCategoryData, translationLanguage, navigate);
  };
  return (
    <>
      {loading ? (
        <>Loading...</>
      ) : (
        <div className="px-5 content-body">
          <div className="card">
            <div className="card-header">
              <div className="flex justify-start ">
                <div className="flex-shrink-0 w-16 h-16">
                  <img
                    className="w-16 h-16 transition duration-200 ease-in transform rounded-full cursor-pointer sm:hover:scale-125 hover:z-50"
                    src={
                      !displayIconId
                        ? "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60"
                        : `http://localhost:9191/image/file/${displayIconId}`
                    }
                    alt=""
                  />
                </div>
                <div className="mt-2 ml-4">
                  <h4 className="card-title">
                    {formTitle ? formTitle : "Title"}
                  </h4>
                  <p className="py-1 text-sm">Model category details</p>
                </div>
              </div>
              <div className="flex">
                <div
                  className="flex px-3 py-1 text-indigo-800 border-2 border-indigo-800 cursor-pointer rounded-3xl hover:bg-indigo-100"
                  onClick={(e) => {
                    e.preventDefault();
                    navigate("/model-category");
                  }}
                >
                  <Backspace width={20} />
                  <button className="pl-2"> Back</button>
                </div>
                <div
                  className={
                    disableSaveButton
                      ? "px-3 py-1 mx-2 text-indigo-800 border-2 border-indigo-800 rounded-3xl bg-indigo-100"
                      : "px-3 py-1 mx-2 text-indigo-800 border-2 border-indigo-800 rounded-3xl hover:bg-indigo-100"
                  }
                >
                  <button disabled={disableSaveButton} onClick={handleSave}>
                    Save
                  </button>
                </div>
              </div>
            </div>

            <div className="card-body">
              {/* Nav tabs */}
              <div className="default-tab">
                <ul className="nav nav-tabs" role="tablist">
                  <li className="nav-item">
                    <a
                      className="nav-link active"
                      data-toggle="tab"
                      href="#default"
                    >
                      Default
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      data-toggle="tab"
                      href="#basic-info"
                    >
                      Basic info
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link" data-toggle="tab" href="#logo">
                      Logo
                    </a>
                  </li>
                </ul>
                <div className="tab-content">
                  <div
                    className="tab-pane fade active show"
                    id="default"
                    role="tabpanel"
                  >
                    <div className="row">
                      <div className="pt-10 col-sm-10">
                        <div className="flex py-2">
                          <label
                            htmlFor="defaultName"
                            className="w-40 text-slate-600"
                          >
                            Default name{" "}
                          </label>
                          <input
                            type="text"
                            className="w-full mr-10 rounded-2xl border-slate-300"
                            value={defaultName}
                            onChange={(e) => {
                              e.preventDefault();
                              setFormTitle(e.target.value);
                              setDefaultName(e.target.value);

                              if (e.target.value === "") {
                                setDisableSaveButton(true);
                              } else {
                                setDisableSaveButton(false);
                              }
                            }}
                          />
                        </div>
                        <div className="flex py-2">
                          <label
                            htmlFor="defualtName"
                            className="w-40 text-slate-600"
                          >
                            Parent{" "}
                          </label>
                          <select
                            value={parentId}
                            onChange={(e) => {
                              e.preventDefault();
                              setParentId(parseInt(e.target.value));
                            }}
                            className="w-full mr-10 rounded-2xl border-slate-300"
                          >
                            <option value={0}>Select parent</option>
                            {parentList.map(
                              (parentItem: ModelCategory, index: number) => {
                                return (
                                  <option key={index} value={parentItem.id}>
                                    {parentItem.defaultName}
                                  </option>
                                );
                              }
                            )}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade "
                    id="basic-info"
                    role="tabpanel"
                  >
                    <div className="row">
                      <div className="col-sm-9">
                        <div className="pt-10">
                          <div className="flex py-2">
                            <label
                              htmlFor="defualtName"
                              className="w-40 text-slate-600"
                            >
                              Name{" "}
                            </label>
                            <input
                              type="text"
                              className="w-full mr-10 rounded-2xl border-slate-300"
                              value={name}
                              onChange={(e) => {
                                setName(e.target.value);
                              }}
                            />
                          </div>
                          <div className="flex py-2">
                            <label
                              htmlFor="defualtName"
                              className="w-40 text-slate-600"
                            >
                              Description{" "}
                            </label>
                            <input
                              type="text"
                              className="w-full mr-10 rounded-2xl border-slate-300"
                              value={description}
                              onChange={(e) => {
                                setDescription(e.target.value);
                              }}
                            />
                          </div>
                        </div>
                        <div className="pt-4">
                          <h4>Content</h4>
                          <div className="pt-2">
                            <textarea
                              value={content}
                              className="w-full border-slate-300 rounded-xl"
                              name="content"
                              id="content"
                              rows={10}
                              onChange={(e) => {
                                setContent(e.target.value);
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-3">
                        <fieldset className="form-group">
                          <div className="row pt-10">
                            <div className="col-sm-9">
                              <label className="col-form-label  pt-0">
                                Language
                              </label>
                              <div className="form-check">
                                {language.language === "uz" ? (
                                  <input
                                    className="form-check-input cursor-pointer"
                                    id="lang-radio"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="uz"
                                    defaultChecked={true}
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelCategoryByIdAndTranslationLanguage(
                                        modelCategoryItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                ) : (
                                  <input
                                    className="form-check-input cursor-pointer"
                                    id="lang-radio"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="uz"
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelCategoryByIdAndTranslationLanguage(
                                        modelCategoryItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                )}
                                <label className="form-check-label">
                                  Uzbek
                                </label>
                              </div>
                              <div className="form-check cursor-pointer">
                                {language.language === "ru" ? (
                                  <input
                                    id="lang-radio"
                                    className="form-check-input cursor-pointer"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="ru"
                                    defaultChecked={true}
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelCategoryByIdAndTranslationLanguage(
                                        modelCategoryItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                ) : (
                                  <input
                                    id="lang-radio"
                                    className="form-check-input cursor-pointer"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="ru"
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelCategoryByIdAndTranslationLanguage(
                                        modelCategoryItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                )}
                                <label className="form-check-label">
                                  Russian
                                </label>
                              </div>
                              <div className="form-check disabled">
                                {language.language === "en" ? (
                                  <input
                                    id="lang-radio"
                                    className="form-check-input cursor-pointer"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="en"
                                    defaultChecked={true}
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelCategoryByIdAndTranslationLanguage(
                                        modelCategoryItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                ) : (
                                  <input
                                    id="lang-radio"
                                    className="form-check-input cursor-pointer"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="en"
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelCategoryByIdAndTranslationLanguage(
                                        modelCategoryItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                )}
                                <label className="form-check-label">
                                  English
                                </label>
                              </div>
                            </div>
                          </div>
                        </fieldset>
                      </div>
                    </div>
                  </div>
                  <div className="tab-pane fade" id="logo">
                    <div className="pt-4">
                      {/* <div className="flex"> */}
                      {imageSettings.map(
                        (item: ImageSettingDto, index: number) => {
                          return (
                            <div key={index} className="flex p-1">
                              <div className="px-2">
                                <label className="block px-2 text-sm font-medium text-gray-700">
                                  {item.fieldDisplayName}
                                </label>
                                <div className="flex justify-center px-6 pt-5 pb-6 mt-1 border-2 border-gray-300 border-dashed rounded-md">
                                  <div className="space-y-1 text-center">
                                    <svg
                                      className="w-12 h-12 mx-auto text-gray-400"
                                      stroke="currentColor"
                                      fill="none"
                                      viewBox="0 0 48 48"
                                      aria-hidden="true"
                                    >
                                      <path
                                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                                        strokeWidth={2}
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                      />
                                    </svg>
                                    <div className="flex text-sm text-gray-600">
                                      <label
                                        htmlFor={`file-upload${index}`}
                                        className="relative font-medium text-indigo-600 bg-white rounded-md cursor-pointer hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                                      >
                                        <span>Upload a file</span>
                                        <input
                                          id={`file-upload${index}`}
                                          name="originalLogo"
                                          type="file"
                                          className="sr-only"
                                          onChange={(e) => {
                                            e.preventDefault();
                                            let file = e.target.files;
                                            console.log(file?.length);

                                            if (
                                              file?.length === 1 &&
                                              validateUpload(
                                                e,
                                                item.width,
                                                item.height
                                              )
                                            ) {
                                              console.log("uploadig ...");
                                              upload(
                                                e,
                                                item.fieldDisplayName,
                                                item.fileTypeCode
                                              );
                                            } else {
                                              alert(
                                                `Please upload only image and with ${item.sizes} size`
                                              );
                                            }
                                          }}
                                          accept="image/jpeg, image/gif, image/png, video/mp4, video/mvk, video/avi"
                                        />
                                      </label>
                                      <p className="pl-1">or drag and drop</p>
                                    </div>
                                    <p className="text-xs text-gray-500">
                                      {item.fileTypeCode} up to 5MB
                                    </p>
                                    <p className="text-xs text-gray-500">
                                      Image size must be {" " + item.sizes}
                                    </p>
                                  </div>
                                </div>
                              </div>
                              {item.fieldDisplayName === "Display icon" ? (
                                displayIconId ? (
                                  <div className="p-4 ">
                                    <img
                                      key={index}
                                      src={
                                        displayLogoImage
                                          ? displayLogoImage
                                          : `http://localhost:9191/file/${displayIconId}`
                                      }
                                      alt=""
                                      width={280}
                                      height={190}
                                    />
                                  </div>
                                ) : (
                                  <div className="p-4 ">
                                    <img
                                      key={index}
                                      src={staticImage}
                                      alt=""
                                      width={280}
                                      height={190}
                                    />
                                  </div>
                                )
                              ) : displayHeaderId ? (
                                item.fileTypeCode === "png" ||
                                item.fileTypeCode === "jpg" ? (
                                  <div className="p-4 ">
                                    <img
                                      src={`http://localhost:9191/file/${displayHeaderId}`}
                                      alt=""
                                      width={280}
                                      height={190}
                                    />
                                  </div>
                                ) : (
                                  <div className="p-4 ">
                                    <ReactPlayer
                                      playing
                                      url={`http://localhost:9191/file/${displayHeaderId}`}
                                      width={300}
                                      height={300}
                                    />
                                  </div>
                                )
                              ) : item.fileTypeCode === "png" ||
                                item.fileTypeCode === "jpg" ? (
                                <div className="p-4 ">
                                  <img
                                    src={displayHeaderFile}
                                    alt=""
                                    width={280}
                                    height={190}
                                  />
                                </div>
                              ) : (
                                <div className="p-4 ">
                                  <ReactPlayer
                                    playing
                                    url={displayHeaderFile}
                                    width={300}
                                    height={300}
                                  />
                                </div>
                              )}
                            </div>
                          );
                        }
                      )}
                      {/* </div> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default ModelCategoryDetails;
