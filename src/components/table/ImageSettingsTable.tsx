/* eslint-disable no-lone-blocks */
import React,{ useEffect, useState } from "react";
import { ImageSettingDto } from "../../features/user/imageSettingDto";
import { APIKit } from "../../request/requests";
import { ImageType } from "../../features/user/imageType";
import { useAppSelector } from "../../app/hooks";
import { selectLanguage } from "../../features/lang/languageSlice";

function ImageSettingsTable() {
  const language = useAppSelector(selectLanguage);
  const [settingList, setSettingsList] = useState<ImageSettingDto[]>([]);
  const [imageTypeList, setImageTypeList] = useState<ImageType[]>([]);
  const [settingListResult, setSettingListResult] = useState<ImageSettingDto[]>(
    []
  );
  const onEdit = (item: ImageSettingDto) => {
    settingList.forEach((element: ImageSettingDto) => {
      if (
        element.fieldName === item.fieldName &&
        element.objectName === item.objectName
      ) {
        if (item.id) {
          APIKit.put("/image/setting/", element)
            .then(({ data }: any) => {
              // console.log(data.body);
            })
            .catch((error) => {
              console.log(error);
            });
        } else {
          APIKit.post("/image/setting/", element)
            .then(({ data }: any) => {
              // console.log(data.body);
            })
            .catch((error) => {
              console.log(error);
            });
        }
      }
    });
  };
  useEffect(() => {
    APIKit.get("/image/setting/").then(({ data }: any) => {
      data.body.sort(function (a: ImageSettingDto, b: ImageSettingDto) {
        let x = a.objectDisplayName.toLowerCase();
        let y = b.objectDisplayName.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
        return 0;
      });

      setSettingsList(data.body);
      setSettingListResult(data.body);
    });
    APIKit.get("/types", {
      params: { key: "image_types", lang: language.language },
    }).then(({ data }: any) => {
      setImageTypeList(data.body);
    });
  }, []);
  return (
    <>
      {/* table */}
      <div className="flex flex-col py-2">
        <div className="-my-2 overflow-x-auto sm:mx-6 lg:mx-8">
          <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
            <div className="border-b border-gray-200 shadow scrollbar-hide sm:rounded-lg">
              <table className="min-w-full overflow-y-auto divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Display name
                    </th>

                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Width
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Height
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Type
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Actions
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {settingListResult.length > 0 ? (
                    settingListResult.map(
                      (item: ImageSettingDto, index: number) => {
                        return (
                          <tr key={index}>
                            <td className="px-6 py-4 whitespace-nowrap">
                              <div className="flex items-center">
                                <div className="ml-4">
                                  <div className="text-sm font-medium text-gray-900">
                                    {item.objectDisplayName}
                                  </div>
                                  <p>{item.fieldDisplayName}</p>
                                </div>
                              </div>
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap">
                              <div className="text-sm text-gray-900">
                                <input
                                  id={item.objectDisplayName + "_input_width"}
                                  type="number"
                                  defaultValue={item.width}
                                  onChange={(e) => {
                                    settingListResult.forEach(
                                      (settingItem: ImageSettingDto) => {
                                        if (
                                          settingItem.objectName ===
                                            item.objectName &&
                                          item.fieldName ===
                                            settingItem.fieldName
                                        ) {
                                          settingItem["width"] =
                                            e.target.valueAsNumber;
                                        }
                                      }
                                    );
                                    setSettingsList(settingListResult);
                                    setSettingListResult(settingList);
                                  }}
                                  className="w-20 rounded-2xl border-slate-300"
                                />
                              </div>
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap">
                              <div className="text-sm text-gray-900">
                                <input
                                  id={item.objectDisplayName + "_input_height"}
                                  onChange={(e) => {
                                    settingListResult.forEach(
                                      (settingItem: ImageSettingDto) => {
                                        if (
                                          settingItem.objectName ===
                                            item.objectName &&
                                          item.fieldName ===
                                            settingItem.fieldName
                                        ) {
                                          settingItem["height"] =
                                            e.target.valueAsNumber;
                                        }
                                      }
                                    );
                                    setSettingListResult(settingListResult);
                                  }}
                                  type="number"
                                  defaultValue={item.height}
                                  className="w-20 rounded-2xl border-slate-300"
                                />
                              </div>
                            </td>
                            <td className="px-6 py-4 whitespace-nowrap">
                              <div className="text-sm text-gray-900">
                                <select
                                  key={index}
                                  onChange={(e) => {
                                    const newList = settingList.map(
                                      (element: ImageSettingDto) => {
                                        if (
                                          element.id === item.id ||
                                          (element.fieldName ===
                                            item.fieldName &&
                                            item.objectName ===
                                              element.objectName)
                                        ) {
                                          element["fileTypeCode"] =
                                            e.target.value;

                                          return element;
                                        }
                                        return element;
                                      }
                                    );
                                    setSettingListResult(newList);
                                  }}
                                  id={index + "_select"}
                                  className="rounded-2xl border-slate-300"
                                >
                                  {imageTypeList.length > 0 ? (
                                    imageTypeList.map(
                                      (imageTypeItem: ImageType) => {
                                        return (
                                          <option
                                            key={imageTypeItem.id}
                                            value={imageTypeItem.typeCode}
                                          >
                                            {imageTypeItem.displayName}
                                          </option>
                                        );
                                      }
                                    )
                                  ) : (
                                    <option value={'png'}>
                                      {'PNG'}
                                    </option>
                                  )}
                                </select>
                              </div>
                            </td>
                            <td className="flex justify-start px-6 py-4 text-sm font-medium text-rfirst-letter:ight whitespace-nowrap">
                              <div className="flex justify-start">
                                <a
                                  href="#/"
                                  className="mr-1 shadow btn btn-primary sharp"
                                  onClick={(e) => {
                                    e.preventDefault();
                                    onEdit(item);
                                  }}
                                >
                                  <i className="fa fa-save" />
                                </a>
                              </div>
                            </td>
                          </tr>
                        );
                      }
                    )
                  ) : (
                    <tr>
                      <td colSpan={4}>
                        <p className="flex justify-center">There is no data</p>
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ImageSettingsTable;
