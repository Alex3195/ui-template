import ImageSettingsTable from "../components/table/ImageSettingsTable";
import SettingTypes from "../components/forms/settingTypes/SettingTypes";
import React from 'react';
function SettingsPage() {
  return (
    <div className="content-body ">
      <div className="col-xl-12">
        <div className="card">
          <div className="card-header">
            <div className="flex justify-between w-80">
              <h4 className="card-title">Settings</h4>
            </div>
          </div>
          <div className="card-body">
            <div className="row">
              <div className="col-xl-2 border-r-2">
                <div className="mb-3 nav flex-column nav-pills">
                  <a
                    href="#v-pills-setting-types"
                    data-toggle="pill"
                    className="nav-link active"
                  >
                    Setting types
                  </a>
                  <a
                    href="#v-pills-brand-image-settings"
                    data-toggle="pill"
                    className="nav-link"
                  >
                    Image settings
                  </a>
                </div>
              </div>
              <div className="col-xl-10">
                <div className="tab-content">
                  <div
                    id="v-pills-setting-types"
                    className="tab-pane fade active show"
                  >
                    <SettingTypes />
                  </div>
                  {/* <div id="v-pills-image-types" className="tab-pane fade ">
                    <ImageTypeSettingsTable />
                  </div> */}
                  <div
                    id="v-pills-brand-image-settings"
                    className="tab-pane fade"
                  >
                    <ImageSettingsTable />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SettingsPage;
