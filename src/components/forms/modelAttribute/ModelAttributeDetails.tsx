import React, { useEffect, useState } from "react";
import { NavigateFunction, useLocation, useNavigate } from "react-router";
import { APIKit } from "../../../request/requests";
import { useAppSelector } from "../../../app/hooks";
import { selectLanguage } from "../../../features/lang/languageSlice";
import { Backspace } from "@material-ui/icons";
import { ModelAttribute } from "../../../types/ModelAttribute";
import { LookupData } from "../../../types/LookupData";
import MultiSelectComponent from "../../MultiSelect/MultiSelectComponent";

async function saveData(
  modelAttribute: ModelAttribute,
  language: any,
  navigate: NavigateFunction
) {
  if (modelAttribute.id) {
    APIKit.put("/model/attribute/", modelAttribute)
      .then(({ data }) => {
        console.log(data);
        navigate("/model-attribute");
      })
      .catch(({ respone }) => {
        return respone;
      });
  } else {
    APIKit.post("/model/attribute/", modelAttribute)
      .then(({ data }) => {
        console.log(data);
        navigate("/model-attribute");
      })
      .catch(({ respone }) => {
        return respone;
      });
  }
}
function saveLookupData(dataP: any) {
  APIKit.post("/lookup", dataP).then(({ data }: any) => {
    console.log(data.body);
  });
}
async function searchOption(searchableData: string) {
  return await APIKit.get("/model/attribute/option/autocomplete", {
    params: { keyword: searchableData },
  });
}

function ModelAttributeDetails() {
  const location = useLocation();
  const navigate = useNavigate();
  const language = useAppSelector(selectLanguage);
  const [disableSaveButton, setDisableSaveButton] = useState(true);
  const [disableTypeOption, setDisableTypeOption] = useState(true);
  const [modelAttributeItem, setModelAttributeItem] =
    useState<ModelAttribute>();
  const [formTitle, setFormTitle] = useState("Title");
  const [attributeType, setAttributeType] = useState("");
  const [attributeTypes, setAttributeTypes] = useState<any[]>([]);
  const [unit, setUnit] = useState("");
  const [units, setUnits] = useState<any[]>([]);
  const [defaultDisplayName, setDefaultDisplayName] = useState<any>("");
  const [displayName, setDisplayName] = useState<any>("");
  const [selectedItems, setSelected] = useState<any>([]);
  const [translationLanguage, setTranslationLanguge] = useState(
    language.language
  );
  const [typeValueSelectOrMultiSelect, setTypeValueSelectOrMultiSelect] =
    useState(false);
  const [loading, setLoading] = useState(true);

  const [filteredSuggestions, setFilteredSuggestions] = useState<any[]>([]);
  useEffect(() => {
    if (location.state) {
      let data = location.state as ModelAttribute;
      setDisableTypeOption(false);
      setModelAttributeItem(data);
      setFormTitle(data.defaultDisplayName);
      setDisableSaveButton(data.defaultDisplayName ? false : true);
      setDisplayName(data.displayName ? data.displayName : "");
      setDefaultDisplayName(
        data.defaultDisplayName ? data.defaultDisplayName : ""
      );
      setUnit(data.unit ? data.unit : "");
      setAttributeType(data.attributeType ? data.attributeType : "");
      if (data.attributeType) {
        data.attributeType.includes("select")
          ? setTypeValueSelectOrMultiSelect(true)
          : setTypeValueSelectOrMultiSelect(false);
      }
      APIKit.get(
        `/model/attribute/option/${data.id}/${language.language}`
      ).then(({ data }: any) => {
        setSelected(data.body);
      });
    }

    APIKit.get("/types", {
      params: { key: "attribute", lang: language.language },
    })
      .then(({ data }: any) => {
        setAttributeTypes(data.body);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
    APIKit.get("/types", {
      params: { key: "unit", lang: language.language },
    })
      .then(({ data }: any) => {
        setUnits(data.body);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });

    return () => {
      console.log("destroyed");
    };
  }, [language]);
  const getModelAttributeByIdAndTranslationLanguage = async (
    id: any,
    lang: any
  ) => {
    APIKit.get(`/model/attribute/${id}/${lang}`)
      .then(({ data }): any => {
        setModelAttributeItem(data.body);
        setFormTitle(data.body.defaultDisplayName);
        setDisableSaveButton(data.body.defaultDisplayName ? false : true);
        setDisplayName(data.body.displayName ? data.body.displayName : "");
        setUnit(data.body.unit ? data.body.unit : "");
        setAttributeType(
          data.body.attributeType ? data.body.attributeType : ""
        );
        setDefaultDisplayName(
          data.body.defaultDisplayName ? data.body.defaultDisplayName : ""
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleSave = (e: React.MouseEvent) => {
    e.preventDefault();
    let arrayMap: any[] = [];
    if (selectedItems.length > 0) {
      selectedItems.map((item: any) => {
        let key = item.id;
        let value = item.value;
        let obj = {
          key: key,
          value: value,
        };
        arrayMap.push(obj);
      });
    }

    console.log(arrayMap, "map");

    let modelAttributeData = {
      id: modelAttributeItem?.id,
      defaultDisplayName: defaultDisplayName,
      displayName: displayName,
      unit: unit,
      attributeType: attributeType,
      options: arrayMap.length > 0 ? arrayMap : null,
    } as ModelAttribute;

    saveData(modelAttributeData, translationLanguage, navigate);

    if (translationLanguage) {
      let obj: LookupData = {
        id: null,
        objectName: "i_model_attribute",
        fieldName: "display_name",
        value: displayName,
        locale: translationLanguage,
        key: modelAttributeItem ? modelAttributeItem.id.toString() : "",
      };
      saveLookupData(obj);
    }
  };

  const getOptionData = (text: string, isEnter: boolean) => {
    let data = searchOption(text);
    data.then(({ data }: any) => {
      if (data.body.length > 0) {
        setFilteredSuggestions(data.body);
      } else {
        if (isEnter) {
          let obj = {
            key: "",
            value: text,
          };
          setSelected([...selectedItems, obj]);
        }
      }
    });
  };
  return (
    <>
      {loading ? (
        <>Loading...</>
      ) : (
        <div className="px-5 content-body">
          <div className="card">
            <div className="card-header">
              <div className="flex justify-start ">
                <div className="mt-2 ml-4">
                  <h4 className="card-title">
                    {formTitle ? formTitle : "Title"}
                  </h4>
                  <p className="py-1 text-sm">Model attribute details</p>
                </div>
              </div>
              <div className="flex">
                <div
                  className="flex px-3 py-1 text-indigo-800 border-2 border-indigo-800 cursor-pointer rounded-3xl hover:bg-indigo-100"
                  onClick={(e) => {
                    e.preventDefault();
                    navigate("/model-attribute");
                  }}
                >
                  <Backspace width={20} />
                  <button className="pl-2"> Back</button>
                </div>
                <div
                  className={
                    disableSaveButton
                      ? "px-3 py-1 mx-2 text-indigo-800 border-2 border-indigo-800 rounded-3xl bg-indigo-100"
                      : "px-3 py-1 mx-2 text-indigo-800 border-2 border-indigo-800 rounded-3xl hover:bg-indigo-100"
                  }
                >
                  <button disabled={disableSaveButton} onClick={handleSave}>
                    Save
                  </button>
                </div>
              </div>
            </div>

            <div className="card-body">
              {/* Nav tabs */}
              <div className="default-tab">
                <ul className="nav nav-tabs" role="tablist">
                  <li className="nav-item">
                    <a
                      className="nav-link active"
                      data-toggle="tab"
                      href="#default"
                    >
                      Default
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      data-toggle="tab"
                      href="#basic-info"
                    >
                      Basic info
                    </a>
                  </li>
                </ul>
                <div className="tab-content">
                  <div
                    className="tab-pane fade active show"
                    id="default"
                    role="tabpanel"
                  >
                    <div className="row">
                      <div className="pt-10 col-sm-10">
                        <div className="flex py-2">
                          <label
                            htmlFor="defualtName"
                            className="w-40 text-slate-600"
                          >
                            Default display name{" "}
                          </label>
                          <input
                            type="text"
                            className="w-full mr-10 rounded-2xl border-slate-300"
                            value={defaultDisplayName}
                            onChange={(e) => {
                              e.preventDefault();
                              setFormTitle(e.target.value);
                              setDefaultDisplayName(e.target.value);

                              if (e.target.value === "") {
                                setDisableSaveButton(true);
                              } else {
                                setDisableSaveButton(false);
                              }
                            }}
                          />
                        </div>
                        <div className="flex py-2">
                          <label
                            htmlFor="defaultName"
                            className="w-40 text-slate-600"
                          >
                            Unit{" "}
                          </label>
                          <select
                            id="unit"
                            value={unit}
                            onChange={(e) => {
                              e.preventDefault();
                              setUnit(e.target.value);
                            }}
                            className="w-full mr-10 rounded-2xl border-slate-300"
                          >
                            <option value={""}>Select unit</option>
                            {units.map((unitItem: any, index: number) => {
                              return (
                                <option key={index} value={unitItem.code}>
                                  {unitItem.displayName}
                                </option>
                              );
                            })}
                          </select>
                        </div>
                        <div className="flex py-2">
                          <label
                            htmlFor="defaultName"
                            className="w-40 text-slate-600"
                          >
                            Type{" "}
                          </label>
                          {disableTypeOption ? (
                            <select
                              id="attributeType"
                              value={attributeType}
                              disabled
                              onChange={(e) => {
                                e.preventDefault();
                                setAttributeType(e.target.value);
                                if (e.target.value.includes("select")) {
                                  setTypeValueSelectOrMultiSelect(true);
                                } else {
                                  setTypeValueSelectOrMultiSelect(false);
                                }
                              }}
                              className="w-full mr-10 rounded-2xl border-slate-300"
                            >
                              <option value={""}>Select type</option>
                              {attributeTypes.map(
                                (attributeItem: any, index: number) => {
                                  return (
                                    <option
                                      key={index}
                                      value={attributeItem.code}
                                    >
                                      {attributeItem.displayName}
                                    </option>
                                  );
                                }
                              )}
                            </select>
                          ) : (
                            <select
                              id="attributeType"
                              value={attributeType}
                              onChange={(e) => {
                                e.preventDefault();
                                setAttributeType(e.target.value);
                                if (e.target.value.includes("select")) {
                                  setTypeValueSelectOrMultiSelect(true);
                                } else {
                                  setTypeValueSelectOrMultiSelect(false);
                                }
                              }}
                              className="w-full mr-10 rounded-2xl border-slate-300"
                            >
                              <option value={""}>Select type</option>
                              {attributeTypes.map(
                                (attributeItem: any, index: number) => {
                                  return (
                                    <option
                                      key={index}
                                      value={attributeItem.code}
                                    >
                                      {attributeItem.displayName}
                                    </option>
                                  );
                                }
                              )}
                            </select>
                          )}
                        </div>
                        {typeValueSelectOrMultiSelect ? (
                          <div className="flex py-2">
                            <label
                              htmlFor="defualtName"
                              className="w-40 text-slate-600"
                            >
                              Options{" "}
                            </label>
                            <div className="w-full mr-10">
                              <div className="w-full">
                                <MultiSelectComponent
                                  items={filteredSuggestions}
                                  setItems={setFilteredSuggestions}
                                  optiondata={getOptionData}
                                  selectedItems={selectedItems}
                                  setSelected={setSelected}
                                />
                              </div>
                            </div>
                          </div>
                        ) : (
                          <></>
                        )}
                      </div>
                    </div>
                  </div>
                  <div
                    className="tab-pane fade "
                    id="basic-info"
                    role="tabpanel"
                  >
                    <div className="row">
                      <div className="col-sm-9">
                        <div className="pt-10">
                          <div className="flex py-2">
                            <label
                              htmlFor="defualtName"
                              className="w-40 text-slate-600"
                            >
                              Display name{" "}
                            </label>
                            <input
                              type="text"
                              className="w-full mr-10 rounded-2xl border-slate-300"
                              value={displayName}
                              onChange={(e) => {
                                setDisplayName(e.target.value);
                              }}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-sm-3">
                        <fieldset className="form-group">
                          <div className="row pt-10">
                            <div className="col-sm-9">
                              <label className="col-form-label  pt-0">
                                Language
                              </label>
                              <div className="form-check">
                                {language.language === "uz" ? (
                                  <input
                                    className="form-check-input cursor-pointer"
                                    id="lang-radio"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="uz"
                                    defaultChecked={true}
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelAttributeByIdAndTranslationLanguage(
                                        modelAttributeItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                ) : (
                                  <input
                                    className="form-check-input cursor-pointer"
                                    id="lang-radio"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="uz"
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelAttributeByIdAndTranslationLanguage(
                                        modelAttributeItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                )}
                                <label className="form-check-label">
                                  Uzbek
                                </label>
                              </div>
                              <div className="form-check cursor-pointer">
                                {language.language === "ru" ? (
                                  <input
                                    id="lang-radio"
                                    className="form-check-input cursor-pointer"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="ru"
                                    defaultChecked={true}
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelAttributeByIdAndTranslationLanguage(
                                        modelAttributeItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                ) : (
                                  <input
                                    id="lang-radio"
                                    className="form-check-input cursor-pointer"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="ru"
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelAttributeByIdAndTranslationLanguage(
                                        modelAttributeItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                )}
                                <label className="form-check-label">
                                  Russian
                                </label>
                              </div>
                              <div className="form-check disabled">
                                {language.language === "en" ? (
                                  <input
                                    id="lang-radio"
                                    className="form-check-input cursor-pointer"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="en"
                                    defaultChecked={true}
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelAttributeByIdAndTranslationLanguage(
                                        modelAttributeItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                ) : (
                                  <input
                                    id="lang-radio"
                                    className="form-check-input cursor-pointer"
                                    type="radio"
                                    name="basicInfoLangRadios"
                                    defaultValue="en"
                                    onChange={(e) => {
                                      e.preventDefault();
                                      setTranslationLanguge(e.target.value);
                                      getModelAttributeByIdAndTranslationLanguage(
                                        modelAttributeItem?.id,
                                        e.target.value
                                      );
                                    }}
                                  />
                                )}
                                <label className="form-check-label">
                                  English
                                </label>
                              </div>
                            </div>
                          </div>
                        </fieldset>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default ModelAttributeDetails;
