export interface TopicTheme {
    id: number;
    defaultTitle: string;
    title: string;
    subTitle: string;
    content: string;
    imageFileId: number;
}