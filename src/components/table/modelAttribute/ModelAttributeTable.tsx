import { ArrowLeft, ArrowRight } from "@material-ui/icons";
import React, { Fragment, useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { useAppSelector } from "../../../app/hooks";
import { selectLanguage } from "../../../features/lang/languageSlice";
import { APIKit } from "../../../request/requests";
import { PlusCircleIcon, TrashIcon } from "@heroicons/react/outline";
import { Menu, Transition } from "@headlessui/react";
import { ChevronDownIcon, PencilAltIcon } from "@heroicons/react/solid";
import { ModelAttribute } from "../../../types/ModelAttribute";

function classNames(...classes: any) {
  return classes.filter(Boolean).join(" ");
}
function previousPage(pageNum: any, setPageNum: any) {
  if (pageNum > 1) {
    setPageNum(pageNum - 1);
  }
}
function nextPage(pageNum: any, setPageNum: any, maxPage: number) {
  if (pageNum < maxPage) {
    setPageNum(pageNum + 1);
  }
}
async function fetchModelAttribute(onSuccess: any, onFailure: any) {
  APIKit.get("/model/attribute/").then(onSuccess).catch(onFailure);
}
async function dropUser(id: any, onFailure: any) {
  await APIKit.delete(`/model/attribute/${id}`)
    .then((res) => {
      console.log(res.data);
    })
    .catch(onFailure);
}
function searchFromArray(data: any, value: any) {
  return data.filter(
    (item: any) =>
      item.defaultName.toLowerCase().indexOf(value.toLowerCase()) !== -1
  );
}

function ModelAttributeTable() {
  const [paging, setPaging] = useState<any>(5);
  const [pages, setPages] = useState<any>(1);
  const [pageNum, setPageNum] = useState<number>(1);
  const [searchData, setSearchData] = useState("");
  const [resultSerach, setResultSearch] = useState<any[]>([]);
  const [modelAttributes, setModeAttributes] = useState<ModelAttribute[]>([]);
  const language = useAppSelector(selectLanguage);
  const navigate = useNavigate();
  useEffect(() => {
    fetchModelAttribute(onSuccess, onFailure);
    modelAttributes.length % paging !== 0
      ? (modelAttributes.length / paging + 1).toFixed()
      : (modelAttributes.length / paging).toFixed();
  }, [language]);
  const addForm = () => {
    navigate("/model-attribute-item");
  };
  const editForm = (item: any) => {
    navigate("/model-attribute-item", { state: item });
  };
  const onSuccess = ({ data }: any) => {
    console.log(data.body);

    data.body.sort(function (a: ModelAttribute, b: ModelAttribute) {
      let x = a.defaultDisplayName.toLowerCase();
      let y = b.defaultDisplayName.toLowerCase();
      if (x < y) {
        return -1;
      }
      if (x > y) {
        return 1;
      }
      return 0;
    });
    setResultSearch(data.body);
    setModeAttributes(data.body);
  };

  const onFailure = (errors: any) => {
    console.log(errors.response);
  };
  const drop = (item: any) => {
    dropUser(item.id, onFailure);
    let index = modelAttributes.indexOf(item);
    modelAttributes.splice(index, 1);
  };
  return (
    <div className="content-body">
      {/* buttons */}
      <div className="flex py-5 lg:items-center lg:justify-between lg:mx-16">
        <div className="flex-1 min-w-0">
          <h2 className="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate">
            Model Attributes{" "}
          </h2>
        </div>
        <div className="flex mt-5 lg:mt-0 lg:ml-4">
          <span className="hidden sm:block">
            <button
              type="button"
              onClick={(e) => {
                e.preventDefault();
                addForm();
              }}
              className="inline-flex items-center px-4 py-2 text-sm font-medium text-white bg-blue-800 border border-gray-300 rounded-md shadow-sm hover:bg-blue-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <PlusCircleIcon
                className="w-5 h-5 mr-2 -ml-1 text-white"
                aria-hidden="true"
              />
              Add
            </button>
          </span>

          {/* Dropdown */}
          <Menu as="span" className="relative ml-3 sm:hidden">
            <Menu.Button className="inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              More
              <ChevronDownIcon
                className="w-5 h-5 ml-2 -mr-1 text-gray-500"
                aria-hidden="true"
              />
            </Menu.Button>

            <Transition
              as={Fragment}
              enter="transition ease-out duration-200"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Menu.Items className="absolute right-0 w-48 py-1 mt-2 -mr-1 origin-top-right bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                <Menu.Item>
                  {({ active }) => (
                    <div
                      className={classNames(
                        active ? "bg-gray-100 cursor-pointer" : "",
                        "block px-4 py-2 text-sm text-gray-700 cursor-pointer"
                      )}
                      onClick={addForm}
                    >
                      Add
                    </div>
                  )}
                </Menu.Item>
              </Menu.Items>
            </Transition>
          </Menu>
        </div>
      </div>
      {/* table */}
      <div className="flex flex-col">
        <div className="-my-2 overflow-x-auto sm:mx-6 lg:mx-8">
          <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
            <div className="border-b border-gray-200 shadow scrollbar-hide sm:rounded-lg">
              <div className="flex justify-end pr-10 pt-4">
                <div className="input-group search-area d-xl-inline-flex d-none pt-2">
                  <div className="input-group-append">
                    <span className="input-group-text">
                      <div className="cursor-pointer">
                        <i className="flaticon-381-search-2" />
                      </div>
                    </span>
                  </div>
                  <input
                    type="text"
                    className="form-control"
                    onChange={(e) => {
                      if (e.target.value === "") {
                        setResultSearch(modelAttributes);
                      } else {
                        setSearchData(e.target.value);
                        let result = searchFromArray(
                          modelAttributes,
                          searchData
                        );
                        setResultSearch(result);
                      }
                    }}
                    placeholder="search"
                  />
                </div>
              </div>
              <table className="min-w-full overflow-y-auto divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="text-xs font-medium tracking-wider text-left text-gray-500 uppercase lg:px-6 lg:py-3"
                    >
                      Display name
                    </th>
                    <th
                      scope="col"
                      className="text-xs font-medium tracking-wider text-left text-gray-500 uppercase lg:px-6 lg:py-3"
                    >
                      Unit
                    </th>
                    <th
                      scope="col"
                      className="text-xs font-medium tracking-wider text-center text-gray-500 uppercase lg:px-6 lg:py-3"
                    >
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {resultSerach.map(
                    (modelAttribute: ModelAttribute, index: number) => {
                      if (
                        index < pageNum * paging &&
                        index >= (pageNum - 1) * paging
                      )
                        return (
                          <tr
                            key={index}
                            className={
                              index % 2 === 1
                                ? "bg-slate-100 cursor-pointer"
                                : "bg-slate-50 cursor-pointer"
                            }
                          >
                            <td
                              className=" lg:px-6 lg:py-4 whitespace-nowrap"
                              onClick={(e) => {
                                e.preventDefault();
                                editForm(modelAttribute);
                              }}
                            >
                              <div className="flex items-center">
                                <div className="ml-4">
                                  <div className="text-sm font-medium text-gray-900">
                                    {modelAttribute.displayName?modelAttribute.displayName:modelAttribute.defaultDisplayName}
                                  </div>
                                  <div className="text-sm text-gray-500">
                                    {modelAttribute.attributeType}
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td
                              className=" lg:py-4 lg:px-6"
                              onClick={(e) => {
                                e.preventDefault();
                                editForm(modelAttribute);
                              }}
                            >
                              <div className="text-sm text-gray-900 line-clamp-2">
                                <p className="mt-3">{modelAttribute.unit}</p>
                              </div>
                            </td>

                            <td className="flex justify-end px-6 py-4 text-sm font-medium text-rfirst-letter:ight whitespace-nowrap ">
                              <div
                                className="cursor-pointer"
                                onClick={(e) => {
                                  e.preventDefault();
                                  editForm(modelAttribute);
                                }}
                              >
                                <span className="hidden ml-3 sm:block">
                                  <button
                                    key={index + "edit"}
                                    id={index + "edit"}
                                    type="button"
                                    className="inline-flex items-center px-2 text-sm font-medium text-blue-300 border-2 border-blue-300 rounded-md shadow-sm lg:py-1 bg-slate-50 hover:bg-blue-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                  >
                                    <PencilAltIcon
                                      className="w-5 h-5 mr-2 -ml-1 text-blue-300"
                                      aria-hidden="true"
                                    />
                                    Edit
                                  </button>
                                </span>
                              </div>
                              <div
                                className="cursor-pointer"
                                onClick={(e) => {
                                  e.preventDefault();
                                  drop(modelAttribute);
                                }}
                              >
                                <span className="hidden ml-3 sm:block">
                                  <button
                                    key={index + "delete"}
                                    id={index + "delete"}
                                    type="button"
                                    className="inline-flex items-center px-2 text-sm font-medium text-blue-300 border-2 border-blue-300 rounded-md shadow-sm lg:py-1 bg-slate-50 hover:bg-blue-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                  >
                                    <TrashIcon
                                      className="w-5 h-5 mr-2 -ml-1 text-blue-300"
                                      aria-hidden="true"
                                    />
                                    Drop
                                  </button>
                                </span>
                              </div>
                            </td>
                          </tr>
                        );
                    }
                  )}
                </tbody>
              </table>
              <div className="relative flex justify-end h-16 border-t-2">
                <div className="px-2 pt-3">Rows per page:</div>
                <div className="flex-row justify-end pt-2">
                  <select
                    className="border-gray-300 rounded-lg border-1"
                    value={paging}
                    onChange={(e) => {
                      setPaging(e.target.value);
                      let val = parseInt(e.target.value);
                      let pagesNum =
                        modelAttributes.length % val !== 0
                          ? (modelAttributes.length / val + 1).toFixed()
                          : (modelAttributes.length / val).toFixed();
                      setPages(pagesNum);
                    }}
                    name="paging"
                    id="pageNums"
                  >
                    <option value={5}>5</option>
                    <option value={10}>10</option>
                    <option value={25}>25</option>
                    <option value={50}>50</option>
                    <option value={100}>100</option>
                  </select>
                </div>
                <div className="flex-row justify-end lg:py-3 lg:px-10">
                  {pageNum} of {pages}
                </div>
                <div className="flex-row justify-end lg:py-3 lg:px-10">
                  <ArrowLeft
                    onClick={(e) => {
                      e.preventDefault();
                      previousPage(pageNum, setPageNum);
                    }}
                    className="cursor-pointer"
                  />
                  <ArrowRight
                    onClick={(e) => {
                      e.preventDefault();
                      let maxPage =
                        modelAttributes.length % paging === 1
                          ? modelAttributes.length / paging + 1
                          : modelAttributes.length / paging;
                      nextPage(pageNum, setPageNum, maxPage);
                    }}
                    className="cursor-pointer"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModelAttributeTable;
