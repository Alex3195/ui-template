export function authHeader(user) {

    console.log("authorization header username", user)
    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}
