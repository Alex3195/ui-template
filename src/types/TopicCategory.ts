export interface TopicCategory {
  id: number;
  defaultTitle: string;
  title: string;
  subTitle: string;
  displayIconId: number;
  parentCategoryId: number;
  modelCategoryId: number[];
}
