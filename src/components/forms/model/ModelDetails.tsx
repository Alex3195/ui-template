import React, { useEffect } from "react";
import ItemDetails from "./ItemDetails";
import { useState } from "react";
import { Model } from "../../../types/Model";
import { APIKit } from "../../../request/requests";
import { useLocation } from "react-router";
import { useAppSelector } from "../../../app/hooks";
import { selectLanguage } from "../../../features/lang/languageSlice";

function ModelDetails() {
  const location = useLocation();
  const langugae = useAppSelector(selectLanguage);
  const [modelItem, setModelItem] = useState<Model>();
  const [loadingCategory, setLoadingCategory] = useState(true);
  const [loadingBrand, setLoadingBrand] = useState(true);
  const [loading, setLoading] = useState(true);
  const [translationLanguage, setTranslationLanguage] = useState(
    langugae.language
  );
  useEffect(() => {
    if (location.state) {
      let model = location.state as Model;
      setModelItem(model);
    }

    if (modelItem){
      getModelByIdAndTranslationLanguage(modelItem.id, translationLanguage);
    }
  }, [translationLanguage, loadingBrand, loadingCategory, loading]);

  const getModelByIdAndTranslationLanguage = async (id: any, lang: any) => {
    APIKit.get(`/model/${id}/${lang}`)
      .then(({ data }): any => {
        setModelItem(data.body);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <>
      <ItemDetails
        item={modelItem}
        setItem={setModelItem}
        subTitle={"Model detils"}
        translationLanguage={translationLanguage}
        setTranslationLanguage={setTranslationLanguage}
      />
    </>
  );
}

export default ModelDetails;
