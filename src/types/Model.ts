export interface Model {
  id: number;
  defaultName: string;
  name: string;
  description: string;
  brandId: null | number;
  categoryId: null | number;
  content: string;
  brandName: string;
  categoryName: string;

}
