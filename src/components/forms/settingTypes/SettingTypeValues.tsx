import React, { useEffect, useState } from "react";
import { APIKit } from "../../../request/requests";
import { SettingTypes } from "../../../types/SettingTypes";

function SettingTypeValues({ setModalState, keyCode, title, editItem }: any) {
  const [name, setName] = useState("");
  const [value, setValue] = useState("");
  const handleSubmit = () => {
    let typeObject = {
      id: editItem ? editItem.id : null,
      displayName: name,
      code: value,
      key: keyCode,
    } as SettingTypes;
    console.log(typeObject);

    if (editItem&&editItem.id) {
      APIKit.put("/types", typeObject)
        .then(({ data }: any) => {
          setModalState(false);
          setName("");
          setValue("");
        })
        .catch((error) => {
          console.log("====================================");
          console.log(error);
          console.log("====================================");
        });
    } else {
      APIKit.post("/types", typeObject)
        .then(({ data }: any) => {
          setModalState(false);
          setName("");
          setValue("");
        })
        .catch((error) => {
          console.log("====================================");
          console.log(error);
          console.log("====================================");
        });
    }
  };
  useEffect(() => {
    if (editItem) {
      setName(editItem.displayName);
      setValue(editItem.code);
    }
  }, []);
  return (
    <div className="col-xl-12 col-lg-12 mt-10">
      <div className="card">
        <div className="card-header">
          <h4 className="card-title">{title}</h4>
        </div>
        <div className="card-body  ">
          <div className="basic-form min-w-fit">
            <div>
              <div className="form-group row">
                <label className="col-sm-3 col-form-label">Display name</label>
                <div className="col-sm-9">
                  <input
                    value={name}
                    required
                    onChange={(e) => {
                      e.preventDefault();
                      setName(e.target.value);
                    }}
                    type="text"
                    className="text-black form-control"
                    placeholder="Display name"
                  />
                </div>
              </div>
              <div className="form-group row">
                <label className="col-sm-3 col-form-label">Key code</label>
                <div className="col-sm-9">
                  <input
                    required
                    value={value}
                    onChange={(e) => {
                      e.preventDefault();
                      setValue(e.target.value);
                    }}
                    type="text"
                    className="text-black form-control"
                    placeholder="key code"
                  />
                </div>
              </div>
              <div className="flex justify-end">
                <button
                  onClick={(e) => {
                    handleSubmit();
                  }}
                  className="p-2 pl-4 pr-4 text-xl text-white bg-blue-500 border-2 rounded-xl hover:bg-blue-600 "
                >
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SettingTypeValues;
