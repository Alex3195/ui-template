import React, { useEffect } from "react";
import { Route, Routes } from "react-router";
import { useAppSelector } from "./app/hooks";
import UserTable from "./components/table/UserTable";
import { selectUser } from "./features/user/userSlice";
import enFlag from "./components/navbar/img/us-flag.png";
import BrandTable from "./components/table/BrandTable";
import LoginTemplate from "./components/forms/userForm/LoginTemplate";
import SideBar from "./components/navbar/sidebar/SideBar";
import Header from "./components/navbar/header/Header";
import Dashboard from "./components/dashboard/Dashboard";
import BrandDetails from "./components/forms/brandForm/BrandDetails";
import SettingsPage from "./pages/SettingsPage";
import UserDetails from "./components/forms/userForm/UserDetails";
import ModelCatgoryTable from "./components/table/modelCategory/ModelCatgoryTable";
import ModelCategoryDetails from "./components/forms/modelCategoryForm/ModelCategoryDetails";
import ModelPage from "./pages/ModelPage";
import ModelDetails from "./components/forms/model/ModelDetails";
import TopicCategoryPage from "./pages/TopicCategoryPage";
import TopicCategoryDetails from "./components/forms/topicCategory/TopicCategoryDetails";
import TopicPage from "./pages/TopicPage";
import TopicDetails from "./components/forms/topic/TopicDetails";
import TopicThemePage from "./pages/TopicThemePage";
import TopicThemeDetails from "./components/forms/topicTheme/TopicThemeDetails";
import ModelAttributePage from "./pages/ModelAttributePage";
import ModelAttributeDetails from "./components/forms/modelAttribute/ModelAttributeDetails";

function App() {
  const user = useAppSelector(selectUser);
  useEffect(() => {
    localStorage.setItem("flagIcon", enFlag);
  }, []);
  return (
    <>
      {user.user ? (
        <>
          <Header />
          ``
          <SideBar />{" "}
        </>
      ) : (
        <></>
      )}
      <Routes>
        <Route index element={!user.user ? <LoginTemplate /> : <Dashboard />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/users" element={<UserTable />} />
        <Route path="/brand" element={<BrandTable />} />
        <Route path="/model-category" element={<ModelCatgoryTable />} />
        <Route path="/brand-item" element={<BrandDetails />} />
        <Route path="/model-category-item" element={<ModelCategoryDetails />} />
        <Route path="/user-form" element={<UserDetails />} />
        <Route path="/settings/*" element={<SettingsPage />} />
        <Route path="/model" element={<ModelPage />} />
        <Route path="/model-item" element={<ModelDetails />} />
        <Route path="/model-attribute" element={<ModelAttributePage />} />
        <Route
          path="/model-attribute-item"
          element={<ModelAttributeDetails />}
        />
        <Route path="/topic-category" element={<TopicCategoryPage />} />
        <Route path="/topic-category-item" element={<TopicCategoryDetails />} />
        <Route path="/topic" element={<TopicPage />} />
        <Route path="/topic-item" element={<TopicDetails />} />
        <Route path="/topic-theme" element={<TopicThemePage />} />
        <Route path="/topic-theme-item" element={<TopicThemeDetails />} />
      </Routes>
    </>
  );
}

export default App;
