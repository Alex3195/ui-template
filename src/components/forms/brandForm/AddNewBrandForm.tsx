import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import { APIKit } from '../../../request/requests';
async function addData(data: any, onSuccess: any, onFailure: any) {
    await APIKit.post('/brand', data)
        .then(onSuccess)
        .catch(onFailure)
}
function AddNewBrandForm() {
    const [defaultBrandName, setDefaultBrandName] = useState('');
    const navigate = useNavigate()
    const handleSubmit = () => {
        const brand = {
            defaultName: defaultBrandName
        }
        addData(brand, onSuccess, onFailure)

    }
    const onSuccess = ({ data }: any) => {
        console.log(data);

        navigate('/brand')
    };

    const onFailure = (errors: any) => {
        console.log(errors.response);
    };
    return (
        <>
            <div className="col-xl-12 col-lg-12">
                <div className="card">
                    <div className="card-header">
                        <h4 className="card-title">Add brand</h4>
                    </div>
                    <div className="card-body">
                        <div className="basic-form">
                            <form>
                                <div className="form-group row">
                                    <label className="col-sm-3 col-form-label">Default name</label>
                                    <div className="col-sm-9">
                                        <input value={defaultBrandName} required onChange={(e) => { e.preventDefault(); setDefaultBrandName(e.target.value) }} type="text" className="text-black form-control" placeholder="Brand name" />
                                    </div>
                                </div>
                                <div className="flex justify-end">
                                    <button onClick={(e) => { handleSubmit() }} type="submit" className="p-2 pl-4 pr-4 text-xl text-white bg-blue-500 border-2 rounded-xl hover:bg-blue-600 ">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default AddNewBrandForm;
