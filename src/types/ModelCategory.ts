export interface ModelCategory {
  id: number;
  defaultName: string;
  name: string;
  description: string;
  content: string;
  displayIconId: number;
  displayHeaderId: number;
  parentId: number;
}
