export interface ImageType{
    id:number|null;
    typeCode:string;
    displayName:string;
}