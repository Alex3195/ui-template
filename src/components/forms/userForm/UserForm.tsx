import React, { useEffect, useState } from "react";
import { UserRespone } from "../../../types/userResponseType";
import { APIKit } from "../../../request/requests";
import { selectUser } from "../../../features/user/userSlice";

async function addUser(user: UserRespone, onSuccess: any, onFailure: any) {
  await APIKit.post("/users/", user).then(onSuccess).catch(onFailure);
}
async function updateUser(user: UserRespone, onSuccess: any, onFailure: any) {
  APIKit.put("/users/", user).then(onSuccess).catch(onFailure);
}
function UserForm({ users, setUsers, selectedUser, setShowModal }: any) {
  const [fullName, setFullName] = useState<any>("");
  const [userName, setUserName] = useState<any>("");
  const [password, setPassword] = useState<any>("");
  const [role, setRole] = useState<any>("MANAGER");
  const [item, setItem] = useState<any>(null);

  const onSuccess = ({ data }: any) => {
    console.log(data);

    if (!selectedUser) {
      setUsers([...users, data.body]);
    } else {
      users.splice(users.indexOf(selectedUser), 1);
      setUsers([...users, data.body]);
    }
    setShowModal(false);
  };

  const onFailure = (errors: any) => {
    console.log(errors.response);
  };

  const handleSubmit = () => {
    if (!selectedUser) {
      let userNew = {
        id: null,
        username: userName,
        fullName: fullName,
        password: password,
        isActive: null,
        role: role,
        image: null,
        token: null,
      } as UserRespone;
      // console.log(selectedUser);

      addUser(userNew, onSuccess, onFailure);
    } else {
      let userNew = {
        id: item.id,
        username: userName,
        fullName: fullName,
        password: password,
        isActive: item.is,
        role: role,
        image: null,
        token: null,
      } as UserRespone;
      updateUser(userNew, onSuccess, onFailure);
      // console.log(userNew);
    }
  };

  useEffect(() => {
    if (selectedUser) {
      let selectedItems = selectedUser as UserRespone;
      setItem(selectedItems);
      setUserName(selectedItems.username);
      setFullName(selectedItems.fullName);
      setRole(selectedItems.role);
    }
  }, []);
  return (
    <>
      <div className="col-xl-12 col-lg-12">
        <div className="card">
          <div className="card-header">
            <h4 className="card-title">User Data</h4>
          </div>
          <div className="card-body bg-slate-200 ">
            <div className="basic-form min-w-fit">
              <div>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Full name</label>
                  <div className="col-sm-9">
                    <input
                      value={fullName}
                      required
                      onChange={(e) => {
                        e.preventDefault();
                        setFullName(e.target.value);
                      }}
                      type="text"
                      className="text-black form-control"
                      placeholder="Full name"
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">User name</label>
                  <div className="col-sm-9">
                    <input
                      required
                      value={userName}
                      onChange={(e) => {
                        e.preventDefault();
                        setUserName(e.target.value);
                      }}
                      type="text"
                      className="text-black form-control"
                      placeholder="Email"
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-sm-3 col-form-label">Password</label>
                  <div className="col-sm-9">
                    <input
                      required
                      value={password}
                      onChange={(e) => {
                        e.preventDefault();
                        setPassword(e.target.value);
                      }}
                      type="password"
                      className="text-black form-control"
                      placeholder="Password"
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label htmlFor="" className="col-sm-3 col-form-label">
                    Role
                  </label>
                  <div className="col-sm-9">
                    <select
                      value={role}
                      onChange={(e) => {
                        e.preventDefault();
                        setRole(e.target.value);
                      }}
                      className="pt-1 text-black form-control form-control-lg default-select"
                    >
                      <option value={"ADMINISTRATOR"}>Administrator</option>
                      <option value={"MANAGER"}>Manager</option>
                      <option value={"MODERATOR"}>Moderator</option>
                    </select>
                  </div>
                </div>
                <div className="flex justify-end">
                  <button
                    onClick={(e) => {
                      handleSubmit();
                    }}
                    className="p-2 pl-4 pr-4 text-xl text-white bg-blue-500 border-2 rounded-xl hover:bg-blue-600 "
                  >
                    Save
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default UserForm;
<>
  {/* <div className="flex justify-center m-5">
                <div className="p-5 mt-10 border-2 rounded-lg sm:mt-0 bg-gradient-to-b from-slate-200 to-slate-50">
                    <div className="md:grid md:grid-cols-3 md:gap-6">
                        <div className="md:col-span-1">
                            <div className="px-4 sm:px-0">
                                <h3 className="text-lg font-medium leading-6 text-gray-900">{item ? 'Edit user' : 'Create user'}</h3>
                                <p className="mt-1 text-sm text-gray-600">Extra info.</p>
                            </div>
                        </div>
                        <div className="mt-5 md:mt-0 md:col-span-2">
                            <form>
                                <div className="overflow-hidden shadow sm:rounded-md">
                                    <div className="px-4 py-5 bg-white sm:p-6">
                                        <div className="grid grid-cols-6 gap-6">
                                            <div className="col-span-6 sm:col-span-6">
                                                <label htmlFor="first-name" className="block text-sm font-medium text-gray-700">
                                                    First name
                                                </label>
                                                <input
                                                    type="text"
                                                    id="first-name"
                                                    autoComplete="given-name"
                                                    required
                                                    value={fullName}
                                                    onChange={(e) => { setFullName(e.target.value) }}
                                                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-6">
                                                <label htmlFor="last-name" className="block text-sm font-medium text-gray-700">
                                                    User name
                                                </label>
                                                <input
                                                    type="text"
                                                    id="user-name"
                                                    autoComplete="user-name"
                                                    required
                                                    value={userName}
                                                    onChange={(e) => { setUserName(e.target.value) }}
                                                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                                />
                                            </div>

                                            <div className="col-span-6 sm:col-span-3">
                                                <label htmlFor="country" className="block text-sm font-medium text-gray-700">
                                                    Password
                                                </label>

                                                <input
                                                    type="password"
                                                    id="password"
                                                    autoComplete="date"
                                                    placeholder="Password"
                                                    required
                                                    onChange={(e) => { setPassword(e.target.value) }}
                                                    className="block w-full mt-1 border-gray-300 rounded-md shadow-sm cursor-pointer focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                                />

                                            </div>

                                            <div className="col-span-6 sm:col-span-3">
                                                <label htmlFor="country" className="block text-sm font-medium text-gray-700">
                                                    Role
                                                </label>
                                                <select
                                                    id="country"
                                                    autoComplete="country-name"
                                                    value={role}
                                                    onChange={(e) => { setRole(e.target.value) }}
                                                    className="block w-full px-3 py-2 mt-1 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                                >
                                                    <option value={'MANAGER'}>MANAGER</option>
                                                    <option value={'ADMINISTRATOR'}>ADMINISTRATOR</option>
                                                    <option value={'MODERATOR'}>MODERATOR</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="px-4 py-3 text-right bg-gray-50 sm:px-6">
                                        <button
                                            type="submit"
                                            onClick={(e) => {
                                                e.preventDefault();
                                                handleSubmit();
                                            }}
                                            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                        >
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div > */}
</>;
