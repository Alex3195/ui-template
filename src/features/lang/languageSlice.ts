import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
export interface LangueageState {
    language: any;
}

const initialState: LangueageState = {
    language: { icon: 'https://api.guest.az/upload/photo/2019/04/29/20190429100358-58707940.png', local: 'en', text: 'English', curent: true },
};


export const languageSlice = createSlice({
    name: 'language',
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        // Use the PayloadAction type to declare the contents of `action.payload`
        setLanguage: (state, action: PayloadAction<any>) => {
            state.language = action.payload;
            localStorage.setItem("lang",action.payload)
        },
    },

});

export const { setLanguage } = languageSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectLanguage = (state: RootState) => state.language;

// We can also write thunks by hand, which may contain both sync and async logic.
// Here's an example of conditionally dispatching actions based on current state.

export default languageSlice.reducer;
