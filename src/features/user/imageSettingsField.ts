export interface ImageSettingField {
  objectName: string;
  fieldName: string;
  column: string;
  table: string;
}
