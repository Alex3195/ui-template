export interface ImageSettingDto {
  id: number;
  objectName: string;
  fieldName: string;
  objectDisplayName: string;
  fieldDisplayName: string;
  typeDisplayName: string;
  sizes: string;
  width: number;
  height: number;
  fileTypeCode: string;
}
