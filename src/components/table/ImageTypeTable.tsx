import { Dialog, Menu, Transition } from "@headlessui/react";
import {
  ChevronDownIcon,
  LinkIcon,
  PencilAltIcon,
  PlusCircleIcon,
  TrashIcon,
  XIcon,
} from "@heroicons/react/solid";
import React, { Fragment, useEffect, useState } from "react";
import { ImageType } from "../../features/user/imageType";
import { APIKit } from "../../request/requests";
import ImageFileTyeSettingForm from "../forms/imageFileType/ImageFileTyeSettingForm";
function classNames(...classes: any) {
  return classes.filter(Boolean).join(" ");
}
function ImageTypeTable() {
  const [modalStateOpen, setModalStateOpen] = useState(false);
  const [imageTypes, setImageTypes] = useState<ImageType[]>([]);
  const [imageTypeItem, setImageTypeItem] = useState<any>(null);
  const onSuccess = ({ data }: any) => {
    setImageTypes(data);
  };
  const onFailure = (errors: any) => {
    console.log(errors.response);
  };
  const onEdit = (item: ImageType) => {
    setImageTypeItem(item);
  };
  useEffect(() => {
    console.log("rendered");

    APIKit.get("/type/images/").then(onSuccess).catch(onFailure);
  }, [imageTypeItem]);
  return (
    <>
      {/* Dialog form */}
      <div>
        <Transition.Root show={modalStateOpen} as={Fragment}>
          <Dialog
            as="div"
            className="fixed inset-0 z-10 overflow-y-auto"
            onClose={setModalStateOpen}
          >
            <div
              className="flex min-h-screen text-center md:block md:px-2 lg:px-4"
              style={{ fontSize: 0 }}
            >
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Dialog.Overlay className="fixed inset-0 hidden transition-opacity bg-gray-500 bg-opacity-75 md:block" />
              </Transition.Child>

              {/* This element is to trick the browser into centering the modal contents. */}
              <span
                className="hidden md:inline-block md:align-middle md:h-screen"
                aria-hidden="true"
              >
                &#8203;
              </span>
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
                enterTo="opacity-100 translate-y-0 md:scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 translate-y-0 md:scale-100"
                leaveTo="opacity-0 translate-y-4 md:translate-y-0 md:scale-95"
              >
                <div className="flex w-full text-base text-left transition transform md:inline-block md:max-w-2xl md:px-4 md:my-8 md:align-middle lg:max-w-4xl">
                  <div className="relative flex items-center w-full px-4 pb-8 overflow-hidden bg-white shadow-2xl pt-14 sm:px-6 sm:pt-8 md:p-6 lg:p-8">
                    <button
                      type="button"
                      className="absolute text-gray-400 top-4 right-4 hover:text-gray-500 sm:top-8 sm:right-6 md:top-6 md:right-6 lg:top-8 lg:right-8"
                      onClick={() => setModalStateOpen(false)}
                    >
                      <span className="sr-only">Close</span>
                      <XIcon className="w-6 h-6" aria-hidden="true" />
                    </button>

                    <div className="grid items-start w-full ">
                      <ImageFileTyeSettingForm
                        item={imageTypeItem}
                        setOpen={setModalStateOpen}
                        imageTypes={imageTypes}
                        setImageTypes={setImageTypes}
                      />
                    </div>
                  </div>
                </div>
              </Transition.Child>
            </div>
          </Dialog>
        </Transition.Root>
      </div>

      {/* buttons */}
      <div className="flex py-5 lg:items-center lg:justify-between lg:mx-16">
        <div className="flex-1 min-w-0">
          <h2 className="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate">
            Image types{" "}
          </h2>
        </div>
        <div className="flex mt-5 lg:mt-0 lg:ml-4">
          <span className="hidden sm:block">
            <button
              type="button"
              onClick={(e) => {
                e.preventDefault();
                setModalStateOpen(true);
              }}
              className="inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <PlusCircleIcon
                className="w-5 h-5 mr-2 -ml-1 text-gray-500"
                aria-hidden="true"
              />
              Add
            </button>
          </span>

          <span className="hidden ml-3 sm:block">
            <button
              type="button"
              onClick={(e) => {
                e.preventDefault();
                console.log("___");
              }}
              className="inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <LinkIcon
                className="w-5 h-5 mr-2 -ml-1 text-gray-500"
                aria-hidden="true"
              />
              Another action
            </button>
          </span>

          {/* Dropdown */}
          <Menu as="span" className="relative ml-3 sm:hidden">
            <Menu.Button className="inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              More
              <ChevronDownIcon
                className="w-5 h-5 ml-2 -mr-1 text-gray-500"
                aria-hidden="true"
              />
            </Menu.Button>

            <Transition
              as={Fragment}
              enter="transition ease-out duration-200"
              enterFrom="transform opacity-0 scale-95"
              enterTo="transform opacity-100 scale-100"
              leave="transition ease-in duration-75"
              leaveFrom="transform opacity-100 scale-100"
              leaveTo="transform opacity-0 scale-95"
            >
              <Menu.Items className="absolute right-0 w-48 py-1 mt-2 -mr-1 origin-top-right bg-white rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                <Menu.Item>
                  {({ active }) => (
                    <a
                      href="#edit-user"
                      className={classNames(
                        active ? "bg-gray-100" : "",
                        "block px-4 py-2 text-sm text-gray-700"
                      )}
                    >
                      Edit
                    </a>
                  )}
                </Menu.Item>
                <Menu.Item>
                  {({ active }) => (
                    <a
                      href="add-user"
                      className={classNames(
                        active ? "bg-gray-100" : "",
                        "block px-4 py-2 text-sm text-gray-700"
                      )}
                    >
                      Add
                    </a>
                  )}
                </Menu.Item>
              </Menu.Items>
            </Transition>
          </Menu>
        </div>
      </div>
      {/* table */}
      <div className="flex flex-col py-5">
        <div className="-my-2 overflow-x-auto sm:mx-6 lg:mx-8">
          <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
            <div className="border-b border-gray-200 shadow scrollbar-hide sm:rounded-lg">
              <table className="min-w-full overflow-y-auto divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Display name
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"
                    >
                      Type
                    </th>
                    <th scope="col" className="px-6 py-3 ">
                      <span className="sr-only">Edit</span>
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {imageTypes.length > 0 ? (
                    imageTypes.map((item: ImageType, index: number) => {
                      return (
                        <tr key={index}>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="flex items-center">
                              <div className="ml-4">
                                <div className="text-sm font-medium text-gray-900">
                                  {item.displayName}
                                </div>
                              </div>
                            </div>
                          </td>
                          <td className="px-6 py-4 whitespace-nowrap">
                            <div className="text-sm text-gray-900">
                              {item.typeCode}
                            </div>
                          </td>
                          <td className="flex justify-end px-6 py-4 text-sm font-medium text-rfirst-letter:ight whitespace-nowrap">
                            <span className="hidden ml-3 sm:block">
                              <button
                                type="button"
                                onClick={(e) => {
                                  e.preventDefault();
                                  onEdit(item);
                                  setModalStateOpen(true);
                                }}
                                className="inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                              >
                                <PencilAltIcon
                                  className="w-5 h-5 mr-2 -ml-1 text-gray-500"
                                  aria-hidden="true"
                                />
                                Edit
                              </button>
                            </span>
                            <span className="hidden ml-3 sm:block">
                              <button
                                type="button"
                                onClick={(e) => {
                                  e.preventDefault();
                                }}
                                className="inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                              >
                                <TrashIcon
                                  className="w-5 h-5 mr-2 -ml-1 text-gray-500"
                                  aria-hidden="true"
                                />
                                Drop
                              </button>
                            </span>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={4}>
                        <p className="flex justify-center">There is no data</p>
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ImageTypeTable;
