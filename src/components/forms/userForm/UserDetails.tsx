import { TrashIcon } from "@heroicons/react/outline";
import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router";
import { APIKit } from "../../../request/requests";
import { UserRespone } from "../../../types/userResponseType";
function uploadAsync(file: File, onSuccessUploadImage: any, onFailure: any) {
  let formData = new FormData();
  formData.append("image", file);
  APIKit.post("/image/file", formData)
    .then(onSuccessUploadImage)
    .catch(onFailure);
}
async function saveUserdData(
  user: UserRespone,
  onSuccessSave: any,
  onFailure: any
) {
  APIKit.post("/users/", user).then(onSuccessSave).catch(onFailure);
}
async function dropUser(id: any, onFailure: any) {
  APIKit.delete(`/users/delete/${id}`)
    .then((res) => {
      console.log(res.data);
    })
    .catch(onFailure);
}
function UserDetails() {
  const location = useLocation();
  const navigate = useNavigate();
  const [disableSaveButton, setDisableSaveButton] = useState(true);
  const [item, setItem] = useState<UserRespone>();
  const [fullName, setFullName] = useState("");
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [previewImage, setPreviewImage] = useState<any>();
  const [uploadedImagId, setUploadedImageId] = useState<any>("");
  const [userNameIsEmpty, setUserNameIsEmpty] = useState(true);
  const [fullNameIsEmpty, setFullNameIsEmpty] = useState(true);
  const [passwordIsEmpty, setPasswordIsEmpty] = useState(true);
  const [showMsgUserNameField, setShowMsgUserNameField] = useState(false);
  const [showMsgFullNameField, setShowMsgFullNameField] = useState(false);
  const [showMsgPasswordField, setShowMsgPasswordField] = useState(false);
  useEffect(() => {
    if (location.state) {
      let data = location.state as UserRespone;
      setItem(data);
      setUserName(data.username);
      setFullName(data.fullName);
      setRole(data.role);
      setUploadedImageId(data.image);
      setDisableSaveButton(false);
      setPreviewImage("http://localhost:9191/image/file/" + data.imageId);
      if (data.username) {
        setShowMsgUserNameField(false);
        setUserNameIsEmpty(false);
      } else {
        setShowMsgUserNameField(false);
        setUserNameIsEmpty(true);
      }

      if (data.fullName) {
        setShowMsgFullNameField(false);
        setFullNameIsEmpty(false);
      } else {
        setShowMsgFullNameField(true);
        setFullNameIsEmpty(true);
      }

      if (data.password) {
        setShowMsgPasswordField(false);
        setPasswordIsEmpty(false);
      } else {
        setShowMsgPasswordField(true);
        setPasswordIsEmpty(true);
        setDisableSaveButton(true);
      }
    }

    return () => {
      console.log("destroyed");
    };
  }, []);

  const validateUpload = (e: any) => {
    return (
      e.target.files[0].type === "image/png" ||
      e.target.files[0].type === "image/gif" ||
      e.target.files[0].type === "image/jpeg"
    );
  };
  const upload = (e: any) => {
    if (e.target.files[0].size <= 10000000) {
      let file = e.target.files[0];
      let render = new FileReader();
      render.onload = function (e) {
        setPreviewImage(e.target?.result);
        uploadAsync(file, onSuccessUploadImage, onFailure);
        console.log(uploadedImagId, "async");
      };
      render.readAsDataURL(file);
    } else {
      e.target.value = "";
      alert("please uploD LESS THAN 10MB");
    }
  };
  const onSuccessUploadImage = ({ data }: any) => {
    setUploadedImageId(data.body);
  };
  const onFailure = (error: any) => {
    console.log(error);
  };
  const onSuccessSave = ({ data }: any) => {
    navigate("/users");
  };
  const handleSave = (e: React.MouseEvent) => {
    e.preventDefault();
    let userData = {
      id: item?.id,
      username: username,
      fullName: fullName,
      password: password,
      role: role,
      imageId: uploadedImagId,
    } as UserRespone;
    console.log("handleSave");
    if (userNameIsEmpty) {
      setShowMsgUserNameField(true);
    } else {
      setShowMsgUserNameField(false);
    }
    if (passwordIsEmpty) {
      setShowMsgPasswordField(true);
    } else {
      setShowMsgPasswordField(false);
    }
    if (fullNameIsEmpty) {
      setShowMsgFullNameField(true);
    } else {
      setShowMsgFullNameField(false);
    }
    if (!userNameIsEmpty && !passwordIsEmpty) {
      if (!fullNameIsEmpty) {
        console.log(userData);
        saveUserdData(userData, onSuccessSave, onFailure);
      }
    }
  };
  return (
    <div className="px-5 content-body">
      <div className="card">
        <div className="card-header">
          <div className="flex justify-start ">
            <div className="flex-shrink-0 w-16 h-16">
              <img
                className="w-16 h-16 transition duration-200 ease-in transform rounded-full cursor-pointer sm:hover:scale-125 hover:z-50"
                src={
                  previewImage ||
                  "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60"
                }
                alt=""
              />
            </div>
            <div className="mt-2 ml-4">
              <h4 className="card-title">{fullName || "Full name"}</h4>
              <p className="py-1 text-sm">{username || "User name"}</p>
            </div>
          </div>
          <div className="flex">
            <div className="flex px-3 py-1 text-indigo-800 border-2 border-indigo-800 cursor-pointer rounded-3xl hover:bg-indigo-100">
              <TrashIcon width={20} />
              <button
                className="pl-2"
                onClick={(e) => {
                  dropUser(item?.id, onFailure);
                }}
              >
                {" "}
                Remove
              </button>
            </div>
            <div
              className={
                disableSaveButton
                  ? "px-3 py-1 mx-2 text-indigo-800 border-2 border-indigo-800 rounded-3xl bg-indigo-100"
                  : "px-3 py-1 mx-2 text-indigo-800 border-2 border-indigo-800 rounded-3xl hover:bg-indigo-100"
              }
            >
              <button disabled={disableSaveButton} onClick={handleSave}>
                Save
              </button>
            </div>
          </div>
        </div>

        <div className="card-body">
          {/* Nav tabs */}
          <div className="default-tab">
            <ul className="nav nav-tabs" role="tablist">
              <li className="nav-item">
                <a
                  className="nav-link active"
                  data-toggle="tab"
                  href="#basic-info"
                >
                  Basic info
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" data-toggle="tab" href="#image">
                  Image
                </a>
              </li>
              <li className="flex justify-between"></li>
            </ul>
            <div className="tab-content">
              <div
                className="tab-pane fade active show"
                id="basic-info"
                role="tabpanel"
              >
                <div className="row">
                  <div className="pt-10 col-sm-10">
                    {showMsgUserNameField ? (
                      <div className="text-red-600">
                        Please enter your user name
                      </div>
                    ) : (
                      ""
                    )}
                    {showMsgFullNameField ? (
                      <div className="text-red-600">
                        Please enter your full name
                      </div>
                    ) : (
                      ""
                    )}
                    <div className="flex py-2">
                      <label
                        htmlFor="defualtName"
                        className="w-40 text-slate-600"
                      >
                        Full name{" "}
                      </label>

                      <input
                        type="text"
                        className="w-full mr-10 rounded-2xl border-slate-300"
                        value={fullName}
                        onChange={(e) => {
                          setFullName(e.target.value);
                          if (e.target.value === "") {
                            setFullNameIsEmpty(true);
                          } else {
                            setFullNameIsEmpty(false);
                          }
                        }}
                      />
                    </div>
                    {showMsgUserNameField ? (
                      <div className="text-red-600">
                        Please enter your user name
                      </div>
                    ) : (
                      ""
                    )}
                    <div className="flex py-2">
                      <label
                        htmlFor="defualtName"
                        className="w-40 text-slate-600"
                      >
                        User name
                      </label>

                      <input
                        type="text"
                        value={username}
                        className="w-full mr-10 rounded-2xl border-slate-300"
                        onChange={(e) => {
                          e.preventDefault();
                          setUserName(e.target.value);
                          if (e.target.value === "") {
                            setUserNameIsEmpty(true);
                            setDisableSaveButton(true);
                          } else {
                            if (!passwordIsEmpty) {
                              setDisableSaveButton(false);
                            }
                            setUserNameIsEmpty(false);
                            setDisableSaveButton(false);
                          }
                        }}
                      />
                    </div>

                    {showMsgPasswordField ? (
                      <div className="text-red-600">
                        Please enter your password
                      </div>
                    ) : (
                      ""
                    )}
                    <div className="flex py-2">
                      <label
                        htmlFor="defualtName"
                        className="w-40 text-slate-600"
                      >
                        Password{" "}
                      </label>
                      <input
                        type="password"
                        className="w-full mr-10 rounded-2xl border-slate-300"
                        value={password}
                        onChange={(e) => {
                          setPassword(e.target.value);
                          if (e.target.value === "") {
                            setPasswordIsEmpty(true);
                            setDisableSaveButton(true);
                          } else {
                            if (!userNameIsEmpty) {
                              setDisableSaveButton(false);
                            } else {
                              setDisableSaveButton(true);
                            }
                            setShowMsgPasswordField(false);
                            setPasswordIsEmpty(false);
                          }
                        }}
                      />
                    </div>
                    <div className="flex py-2">
                      <label
                        htmlFor="defualtName"
                        className="w-40 text-slate-600"
                      >
                        Role{" "}
                      </label>
                      <select
                        value={role}
                        onChange={(e) => {
                          e.preventDefault();
                          setRole(e.target.value);
                        }}
                        className="w-full mr-10 rounded-2xl border-slate-300"
                      >
                        <option value={"ADMINISTRATOR"}>Administrator</option>
                        <option value={"MANAGER"}>Manager</option>
                        <option value={"MODERATOR"}>Moderator</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div className="tab-pane fade" id="image">
                <div className="pt-4">
                  <div className="flex">
                    <div className="px-2">
                      <label className="block px-2 text-sm font-medium text-gray-700">
                        Image
                      </label>
                      <div className="flex justify-center px-6 pt-5 pb-6 mt-1 border-2 border-gray-300 border-dashed rounded-md">
                        <div className="space-y-1 text-center">
                          <svg
                            className="w-12 h-12 mx-auto text-gray-400"
                            stroke="currentColor"
                            fill="none"
                            viewBox="0 0 48 48"
                            aria-hidden="true"
                          >
                            <path
                              d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                              strokeWidth={2}
                              strokeLinecap="round"
                              strokeLinejoin="round"
                            />
                          </svg>
                          <div className="flex text-sm text-gray-600">
                            <label
                              htmlFor="file-upload"
                              className="relative font-medium text-indigo-600 bg-white rounded-md cursor-pointer hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
                            >
                              <span>Upload a file</span>
                              <input
                                id="file-upload"
                                name="originalLogo"
                                type="file"
                                className="sr-only"
                                onChange={(e) => {
                                  e.preventDefault();
                                  let file = e.target.files;
                                  if (file?.length === 1 && validateUpload(e)) {
                                    upload(e);
                                  } else {
                                    alert("Please upload only image");
                                  }
                                }}
                                accept="image/jpeg, image/gif, image/png"
                              />
                            </label>
                            <p className="pl-1">or drag and drop</p>
                          </div>
                          <p className="text-xs text-gray-500">
                            PNG, JPG, GIF up to 10MB
                          </p>
                        </div>
                      </div>
                    </div>
                    {previewImage ? (
                      <div className="pt-7 ">
                        <img
                          src={previewImage}
                          alt=""
                          width={300}
                          height={190}
                        />
                      </div>
                    ) : (
                      <></>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UserDetails;
