import { Save } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
// import { useDispatch } from "react-redux";
// import { useAppSelector } from "../../../app/hooks";
// import {
//   removeAll,
//   selectAttributes,
// } from "../../../features/attribute/attributeSlice";
import { APIKit } from "../../../request/requests";
import { ModelAttribute } from "../../../types/ModelAttribute";
import AttributeValue from "../../forms/model/AttributeValue";

function AttributeTable({ items, setItems, modelId }: any) {
  const [itemId, setItemId] = useState<any>(0);
  const [prevItemId, setPrevItemId] = useState<any>(0);
  const [attributeOptions, setAttributeOptions] = useState<any[]>([]);
  const [attributeObjects, setAttributeObjects] = useState<any[]>([]);
  const [isClick, setIsClick] = useState(false);
  const [isExists, setIsExists] = useState(false);
  const getOptionsFromChildComponent = (options: any[]) => {
    setAttributeOptions(options);
  };
  const saveAttributeData = (item: any) => {
    let obj = attributeObjects.filter((itemAttribute: any) => {
      if (itemAttribute.id === item.id) {
        return itemAttribute;
      }
    })[0];
    let attribute = "";
    obj.attributes.forEach((itemObj: any) => {
      attribute = attribute.concat(itemObj.value + ";");
    });
    let readyObjectForSave = {
      modelId: modelId,
      attributeId: obj.id,
      value: attribute,
    };
    APIKit.post("/model/attribute/value", readyObjectForSave).then(
      ({ data }: any) => {
        console.log(data.body);
      }
    );
  };
  useEffect(() => {
    console.clear();
    if (itemId !== 0 && prevItemId !== itemId) {
      let obj = {
        id: itemId,
        attributes: attributeOptions,
      };
      attributeObjects.forEach((item: any) => {
        if (item.id === itemId) {
          setIsExists(true);
        }
      });
      if (!isExists) {
        setAttributeObjects([...attributeObjects, obj]);
      } else {
        let objArray = attributeObjects.map((item: any) => {
          if (item.id === itemId) {
            return {
              id: itemId,
              attributes: attributeOptions,
            };
          } else {
            return item;
          }
        });
        setAttributeObjects(objArray);
      }
      setPrevItemId(itemId);
    } else {
      let objArray = attributeObjects.map((item: any) => {
        if (item.id === prevItemId) {
          return {
            id: prevItemId,
            attributes: attributeOptions,
          };
        } else {
          return item;
        }
      });
      setAttributeObjects(objArray);
    }
  }, [items, itemId, attributeOptions]);
  return (
    <>
      {/* table */}
      <div className="flex flex-col mt-5">
        <div className="-my-2 overflow-x-auto sm:mx-6 lg:mx-8">
          <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
            <div className="border-b border-gray-200 shadow scrollbar-hide sm:rounded-lg">
              <table className="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th> Type</th>
                    <th>Values</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {items.length > 0 ? (
                    items.map((item: ModelAttribute, index: number) => {
                      return (
                        <tr key={index}>
                          <td>
                            {item.displayName
                              ? item.displayName
                              : item.defaultDisplayName}
                          </td>
                          <td>{item.attributeType}</td>
                          <td
                            onClick={(e) => {
                              e.preventDefault();
                              setItemId(item.id);
                            }}
                          >
                            <AttributeValue
                              item={item}
                              getOptions={getOptionsFromChildComponent}
                            />
                            {/* {attributeValue(item,filteredSuggestions,setFilteredSuggestions,getOptionData,selectedItems,setSelected)} */}
                          </td>
                          <td>
                            <button
                              className="btn btn-sm btn-outline-primary"
                              onClick={(e) => {
                                e.preventDefault();
                                saveAttributeData(item);
                              }}
                            >
                              <Save />
                            </button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr>
                      <td colSpan={4}>
                        <div className="w-full text-center">
                          There is no data
                        </div>
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default AttributeTable;
