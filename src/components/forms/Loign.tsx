import React, { useState } from "react";
import { LockClosedIcon } from "@heroicons/react/solid";
import { useAppDispatch } from "../../app/hooks";
import { login } from "../../features/user/userSlice";
import { User } from "../../types/user";
import { APIKit } from "../../request/requests";
import { UserRespone } from "../../types/userResponseType";
import { useNavigate } from "react-router";

function Loign() {
  const [userLogin, setUserLogin] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useAppDispatch();
  const naivgate = useNavigate();
  const onSuccess = ({ data }: any) => {
    // Set JSON Web Token on success
    let userResponse = {
      id: data.user.id,
      fullName: data.user.fullName,
      username: data.user.username,
      password: data.user.password,
      isActive: data.user.isActive,
      role: data.user.role,
      token: data.token,
      image: data.image,
    } as UserRespone;
    localStorage.setItem("token", data.token);
    dispatch(login(userResponse));
    naivgate("/dashboard");
  };

  const onFailure = (errors: any) => {
    console.log(errors.response);
  };

  const handleSubmit = () => {
    let user: User;
    user = {
      username: userLogin,
      password: password,
    };

    APIKit.post("/login", JSON.stringify(user), {
      headers: { "Content-Type": "application/json" },
    })
      .then(onSuccess)
      .catch(onFailure);
  };
  return (
    <div className="flex items-center justify-center min-h-full px-4 py-12 sm:px-6 lg:px-8">
      <div className="w-full max-w-md space-y-8">
        <div>
          <img
            className="w-auto h-12 mx-auto"
            src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
            alt="Workflow"
          />
          <h2 className="mt-6 text-3xl font-extrabold text-center text-gray-900">
            Welcome Instruction App
          </h2>
          <h2 className="mt-6 text-3xl font-extrabold text-center text-gray-900">
            Sign in to your account
          </h2>
        </div>
        <form
          className="mt-8 space-y-6"
          onSubmit={(e) => {
            e.preventDefault();
            handleSubmit();
          }}
        >
          <input type="hidden" name="remember" defaultValue="true" />
          <div className="-space-y-px rounded-md shadow-sm">
            <div>
              <label htmlFor="email-address" className="sr-only">
                Email address
              </label>
              <input
                id="email-address"
                type="text"
                autoComplete="email"
                value={userLogin}
                onChange={(e) => {
                  e.preventDefault();
                  console.log(e.target.value);
                  setUserLogin(e.target.value);
                }}
                required
                className="relative block w-full px-3 py-2 text-gray-900 placeholder-gray-500 border border-gray-300 rounded-none appearance-none rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Email address"
              />
            </div>
            <div>
              <label htmlFor="password" className="sr-only">
                Password
              </label>
              <input
                id="password"
                type="password"
                autoComplete="current-password"
                value={password}
                onChange={(e) => {
                  e.preventDefault();
                  setPassword(e.target.value);
                }}
                required
                className="relative block w-full px-3 py-2 text-gray-900 placeholder-gray-500 border border-gray-300 rounded-none appearance-none rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Password"
              />
            </div>
          </div>

          <div className="flex items-center justify-between">
            <div className="flex items-center">
              <input
                id="remember-me"
                type="checkbox"
                className="w-4 h-4 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500"
              />
              <label
                htmlFor="remember-me"
                className="block ml-2 text-sm text-gray-900"
              >
                Remember me
              </label>
            </div>

            <div className="text-sm">
              <p className="font-medium text-indigo-600 cursor-pointer hover:text-indigo-500 ">
                Forgot your password?
              </p>
            </div>
          </div>

          <div>
            <button
              type="submit"
              className="relative flex justify-center w-full px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md group hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                <LockClosedIcon
                  className="w-5 h-5 text-indigo-500 group-hover:text-indigo-400"
                  aria-hidden="true"
                />
              </span>
              Sign in
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Loign;
