import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export interface AttributeState {
    attribute: any[];
}

const initialState: AttributeState = {
    attribute: [],
};

export const attributeSlice = createSlice({
    name: "attribute",
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        removeAll: (state) => {
            state.attribute = [];
        },
        // Use the PayloadAction type to declare the contents of `action.payload`
        add: (state, action: PayloadAction<any>) => {
            state.attribute = action.payload
        },
    },
});

export const { add, removeAll } = attributeSlice.actions;

export const selectAttributes = (state: RootState) => state.attribute;

export default attributeSlice.reducer;
