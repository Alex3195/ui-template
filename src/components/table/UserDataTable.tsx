import React from 'react';
import { Card } from '@material-ui/core';
import DataTable from 'react-data-table-component';
import SortIcon from "@material-ui/icons/ArrowDownward";
import { useEffect, useState } from 'react';
import { UserRespone } from '../../types/userResponseType';
import { APIKit } from '../../request/requests';
import { DeleteOutline, EditOutlined } from '@material-ui/icons';
import { useNavigate } from 'react-router';
import { PlusCircleIcon } from '@heroicons/react/outline';

function UserDataTable() {
    const [users, setUsers] = useState<UserRespone[]>([]);
    const [userItem, setUserItem] = useState<UserRespone | null>(null)
    const navigate = useNavigate();
    useEffect(() => {

        APIKit.get('/users/all')
            .then(onSuccess)
            .catch(onFailure)
    }, [])
    const columns = [
        {
            id: 1,
            name: "Name",
            selector: (row: UserRespone) => row.fullName,
            sortable: true,
            reorder: true
        },
        {
            id: 2,
            name: "User name",
            selector: (row: UserRespone) => row.username,
            sortable: true,
            reorder: true
        },
        {
            id: 3,
            name: "Role",
            selector: (row: UserRespone) => row.role,
            sortable: true,
            right: false,
            reorder: true,

        },
        {
            id: 4,
            name: "Action",
            cell: (row: UserRespone) => <div className='flex justify-between mr-2'>
                <div className='flex-col p-2 mr-2 border-2 cursor-pointer rounded-xl bg-amber-200 hover:bg-amber-400' onClick={(e) => { editForm(row) }}><EditOutlined /></div>
                <div className='flex-col p-2 bg-pink-400 border-2 cursor-pointer rounded-xl hover:bg-pink-600' onClick={(e) => { dropUser(row) }} ><DeleteOutline /></div>
            </div>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
            right: false,
        },


    ];
    function dropUser(item: UserRespone) {
        console.log("Drop user");
        APIKit.delete(`/users/delete/${item.id}`)
            .then(res => {
                console.log(res.data);
            })
            .catch(onFailure)

        let index = users.indexOf(item);
        users.splice(index, 1)

    }


    const editForm = (item: UserRespone) => {
        navigate('/user-form', { state: item })

        setUserItem(item);
        console.log(userItem);
    }
    const addForm = () => {
        navigate('/user-form')
    }
    const onSuccess = ({ data }: any) => {
        setUsers(data)
    };

    const onFailure = (errors: any) => {
        console.log(errors.response);
    };

    return (

        <div className="content-body">
            {/* row */}
            <div className="container-fluid">
                <div className='flex justify-end'>
                    <button onClick={(e) => { navigate("/user-form")}} className='flex p-2 text-white bg-blue-800 border-2 hover:bg-blue-900 rounded-xl'>
                        <i className='w-9'><PlusCircleIcon /></i>
                        <strong className='pt-1 pl-2 pr-2'>Add user</strong>
                    </button>
                </div>
            </div>
            <div className="row">
                <div className="col-xl-12 col-lg-12">
                    <Card className='m-5 rounded-sm'>
                        <DataTable
                            title="Users"
                            columns={columns}
                            data={users}
                            defaultSortFieldId={1}
                            sortIcon={<SortIcon />}
                            pagination
                        />
                    </Card>
                </div>
            </div>
        </div>
    );
}

export default UserDataTable;
