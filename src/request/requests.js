import axios from "axios";
// const baseUrl = 'http://192.168.31.29:9191'
const baseUrl = 'http://localhost:9191'
export let APIKit = axios.create({
  baseURL: baseUrl,
  timeout: 10000,
});

// Set JSON Web Token in Client to be included in all calls
APIKit.interceptors.request.use(function (config) {
  console.log('token config');
  const token = localStorage.getItem('token');
  const lang = localStorage.getItem("lang");
  config.headers.Authorization = token ? `Bearer ${token}` : '';
  config.headers['Accept-Language'] = lang ? lang : 'en';
  return config;
});