import React from "react";
import ModelAttributeTable from "../components/table/modelAttribute/ModelAttributeTable";

function ModelAttributePage() {
  return (
    <>
      <ModelAttributeTable />
    </>
  );
}

export default ModelAttributePage;
